import Toybox.Graphics;


module Codex {

    class Theme {

        static const HEADING1_FONT_DEFAULT = Graphics.FONT_MEDIUM;
        static const HEADING1_SPACING_AFTER_DEFAULT = 16;
        static const HEADING1_HORIZONTAL_RULE_DEFAULT = true;
        static const HEADING2_FONT_DEFAULT = Graphics.FONT_SMALL;
        static const HEADING2_SPACING_AFTER_DEFAULT = 16;
        static const HEADING2_HORIZONTAL_RULE_DEFAULT = true;
        static const HEADING3_FONT_DEFAULT = Graphics.FONT_TINY;
        static const HEADING3_SPACING_AFTER_DEFAULT = 8;
        static const HEADING3_HORIZONTAL_RULE_DEFAULT = false;
        static const HEADING4_FONT_DEFAULT = Graphics.FONT_XTINY;
        static const HEADING4_SPACING_AFTER_DEFAULT = 8;
        static const HEADING4_HORIZONTAL_RULE_DEFAULT = false;

        static const IMAGE_SPACING_AFTER_DEFAULT = 8;
        
        static const LIST_FONT_DEFAULT = Graphics.FONT_XTINY;
        static const LIST_SPACING_AFTER_DEFAULT = 16;
        static const LIST_ITEM_FONT_DEFAULT = Graphics.FONT_XTINY;
        static const LIST_ITEM_SPACING_AFTER_DEFAULT = 8;

        static const PARAGRAPH_FONT_DEFAULT = Graphics.FONT_XTINY;
        static const PARAGRAPH_SPACING_AFTER_DEFAULT = 16;

        static const TABLE_SPACING_AFTER_DEFAULT = 16;
        static const TABLE_CELL_PADDING_TOP_BOTTOM_DEFAULT = 0;
        static const TABLE_CELL_PADDING_SIDES_DEFAULT = 3;
        static const TABLE_HEADER_BOTTOM_LINE_THICKNESS_DEFAULT = 2;

        static const MENU_TITLE_HEIGHT_DEFAULT = 92;
        static const MENU_TITLE_FONT_DEFAULT = Graphics.FONT_XTINY;
        static const MENU_ITEM_HEIGHT_DEFAULT = 44;
        static const MENU_ITEM_FONT_DEFAULT = Graphics.FONT_TINY;
        static const MENU_FOCUS_HEIGHT_DEFAULT = 56;
        static const MENU_FOCUS_FONT_DEFAULT = Graphics.FONT_SMALL;

        // Colors
        private var _backgroundColor as Number;
        private var _textColor as Number;
        private var _majorAccentColor as Number;
        private var _minorAccentColor as Number;

        // Header options
        private var _heading1Font as Number;
        private var _heading1SpacingAfter as Number;
        private var _heading1HorizontalRule as Boolean;
        private var _heading2Font as Number;
        private var _heading2SpacingAfter as Number;
        private var _heading2HorizontalRule as Boolean;
        private var _heading3Font as Number;
        private var _heading3SpacingAfter as Number;
        private var _heading3HorizontalRule as Boolean;
        private var _heading4Font as Number;
        private var _heading4SpacingAfter as Number;
        private var _heading4HorizontalRule as Boolean;

        // Image options
        private var _imageSpacingAfter as Number;
        
        // List options
        private var _listFont as Number;
        private var _listSpacingAfter as Number;
        private var _listItemFont as Number;
        private var _listItemSpacingAfter as Number;

        // Paragraph options
        private var _paragraphFont as Number;
        private var _paragraphSpacingAfter as Number;

        // Table options
        private var _tableSpacingAfter as Number;
        private var _tableCellPaddingTopBottom as Number;
        private var _tableCellPaddingSides as Number;
        private var _tableHeaderBottomLineThickness as Number;

        // Menu options
        private var _menuTitleHeight as Number;
        private var _menuTitleFont as Number;
        private var _menuItemHeight as Number;
        private var _menuItemFont as Number;
        private var _menuFocusHeight as Number;
        private var _menuFocusFont as Number;

        function initialize(backgroundColor as Number, textColor as Number, majorAccentColor as Number, 
            minorAccentColor as Number, options as {
            :heading1Font as Number, :heading1SpacingAfter as Number, :heading1HorizontalRule as Boolean,
            :heading2Font as Number, :heading2SpacingAfter as Number, :heading2HorizontalRule as Boolean,
            :heading3Font as Number, :heading3SpacingAfter as Number, :heading3HorizontalRule as Boolean,
            :heading4Font as Number, :heading4SpacingAfter as Number, :heading4HorizontalRule as Boolean,
            :imageSpacingAfter as Number, :listFont as Number, :listSpacingAfter as Number, :listItemFont as Number,
            :listItemSpacingAfter as Number, :paragraphFont as Number, :paragraphSpacingAfter as Number,
            :tableSpacingAfter as Number, :tableCellPaddingTopBottom as Number, :tableCellPaddingSides as Number, 
            :tableHeaderBottomLineThickness as Number,
            :menuTitleHeight as Number, :menuTitleFont as Number, :menuItemHeight as Number,
            :menuItemFont as Number, :menuFocusHeight as Number, :menuFocusFont as Number
            }) {
            /*
            Color and style information for rendering content

            :param backgroundColor: Background color
            :param textColor: Text color
            :param majorAccentColor: Major accent color (horizontal rules, table borders)
            :param minorAccentColor: Minor accent color (background differentiation)
            :param options: Additional options
            */
            _backgroundColor = backgroundColor;
            _textColor = textColor;
            _majorAccentColor = majorAccentColor;
            _minorAccentColor = minorAccentColor;

            _heading1Font = Utils.getDefault(options, :heading1Font, HEADING1_FONT_DEFAULT);
            _heading1SpacingAfter = Utils.getDefault(options, :heading1SpacingAfter, HEADING1_SPACING_AFTER_DEFAULT);
            _heading1HorizontalRule = Utils.getDefault(options, :heading1HorizontalRule, HEADING1_HORIZONTAL_RULE_DEFAULT);
            _heading2Font = Utils.getDefault(options, :heading2Font, HEADING2_FONT_DEFAULT);
            _heading2SpacingAfter = Utils.getDefault(options, :heading2SpacingAfter, HEADING2_SPACING_AFTER_DEFAULT);
            _heading2HorizontalRule = Utils.getDefault(options, :heading2HorizontalRule, HEADING2_HORIZONTAL_RULE_DEFAULT);
            _heading3Font = Utils.getDefault(options, :heading3Font, HEADING3_FONT_DEFAULT);
            _heading3SpacingAfter = Utils.getDefault(options, :heading3SpacingAfter, HEADING3_SPACING_AFTER_DEFAULT);
            _heading3HorizontalRule = Utils.getDefault(options, :heading3HorizontalRule, HEADING3_HORIZONTAL_RULE_DEFAULT);
            _heading4Font = Utils.getDefault(options, :heading4Font, HEADING4_FONT_DEFAULT);
            _heading4SpacingAfter = Utils.getDefault(options, :heading4SpacingAfter, HEADING4_SPACING_AFTER_DEFAULT);
            _heading4HorizontalRule = Utils.getDefault(options, :heading4HorizontalRule, HEADING4_HORIZONTAL_RULE_DEFAULT);

            _imageSpacingAfter = Utils.getDefault(options, :imageSpacingAfter, IMAGE_SPACING_AFTER_DEFAULT);

            _listFont = Utils.getDefault(options, :listFont, LIST_FONT_DEFAULT);
            _listSpacingAfter = Utils.getDefault(options, :listSpacingAfter, LIST_SPACING_AFTER_DEFAULT);
            _listItemFont = Utils.getDefault(options, :listItemFont, LIST_ITEM_FONT_DEFAULT);
            _listItemSpacingAfter = Utils.getDefault(options, :listItemSpacingAfter, LIST_ITEM_SPACING_AFTER_DEFAULT);

            _paragraphFont = Utils.getDefault(options, :paragraphFont, PARAGRAPH_FONT_DEFAULT);
            _paragraphSpacingAfter = Utils.getDefault(options, :paragraphSpacingAfter, PARAGRAPH_SPACING_AFTER_DEFAULT);

            _tableSpacingAfter = Utils.getDefault(options, :tableSpacingAfter, TABLE_SPACING_AFTER_DEFAULT);
            _tableCellPaddingTopBottom = Utils.getDefault(options, :tableCellPaddingTopBottom,
                TABLE_CELL_PADDING_TOP_BOTTOM_DEFAULT);
            _tableCellPaddingSides = Utils.getDefault(options, :tableCellPaddingSides, TABLE_CELL_PADDING_SIDES_DEFAULT);
            _tableHeaderBottomLineThickness = Utils.getDefault(options, :tableHeaderBottomLineThickness,
                TABLE_HEADER_BOTTOM_LINE_THICKNESS_DEFAULT);

            _menuTitleHeight = Utils.getDefault(options, :menuTitleHeight, MENU_TITLE_HEIGHT_DEFAULT);
            _menuTitleFont = Utils.getDefault(options, :menuTitleFont, MENU_TITLE_FONT_DEFAULT);
            _menuItemHeight = Utils.getDefault(options, :menuItemHeight, MENU_ITEM_HEIGHT_DEFAULT);
            _menuItemFont = Utils.getDefault(options, :menuItemFont, MENU_ITEM_FONT_DEFAULT);
            _menuFocusHeight = Utils.getDefault(options, :menuFocusHeight, MENU_FOCUS_HEIGHT_DEFAULT);
            _menuFocusFont = Utils.getDefault(options, :menuFocusFont, MENU_FOCUS_FONT_DEFAULT);
        }

        function getBackgroundColor() as Number {
            return _backgroundColor;
        }

        function setBackgroundColor(value as Number) as Void {
            _backgroundColor = value;
        }

        function getTextColor() as Number {
            return _textColor;
        }

        function setTextColor(value as Number) as Void {
            _textColor = value;
        }

        function getMajorAccentColor() as Number {
            return _majorAccentColor;
        }

        function setMajorAccentColor(value as Number) as Void {
            _majorAccentColor = value;
        }

        function getMinorAccentColor() as Number {
            return _minorAccentColor;
        }

        function setMinorAccentColor(value as Number) as Void {
            _minorAccentColor = value;
        }

        function getHeading1Font() as Number {
            return _heading1Font;
        }

        function setHeading1Font(value as Number) as Void {
            _heading1Font = value;
        }

        function getHeading1SpacingAfter() as Number {
            return _heading1SpacingAfter;
        }

        function setHeading1SpacingAfter(value as Number) as Void {
            _heading1SpacingAfter = value;
        }

        function getHeading1HorizontalRule() as Boolean {
            return _heading1HorizontalRule;
        }

        function setHeading1HorizontalRule(value as Boolean) as Void {
            _heading1HorizontalRule = value;
        }

        function getHeading2Font() as Number {
            return _heading2Font;
        }

        function setHeading2Font(value as Number) as Void {
            _heading2Font = value;
        }

        function getHeading2SpacingAfter() as Number {
            return _heading2SpacingAfter;
        }

        function setHeading2SpacingAfter(value as Number) as Void {
            _heading2SpacingAfter = value;
        }

        function getHeading2HorizontalRule() as Boolean {
            return _heading2HorizontalRule;
        }

        function setHeading2HorizontalRule(value as Boolean) as Void {
            _heading2HorizontalRule = value;
        }

        function getHeading3Font() as Number {
            return _heading3Font;
        }

        function setHeading3Font(value as Number) as Void {
            _heading3Font = value;
        }

        function getHeading3SpacingAfter() as Number {
            return _heading3SpacingAfter;
        }

        function setHeading3SpacingAfter(value as Number) as Void {
            _heading3SpacingAfter = value;
        }

        function getHeading3HorizontalRule() as Boolean {
            return _heading3HorizontalRule;
        }

        function setHeading3HorizontalRule(value as Boolean) as Void {
            _heading3HorizontalRule = value;
        }

        function getHeading4Font() as Number {
            return _heading4Font;
        }

        function setHeading4Font(value as Number) as Void {
            _heading4Font = value;
        }

        function getHeading4SpacingAfter() as Number {
            return _heading4SpacingAfter;
        }

        function setHeading4SpacingAfter(value as Number) as Void {
            _heading4SpacingAfter = value;
        }

        function getHeading4HorizontalRule() as Boolean {
            return _heading4HorizontalRule;
        }

        function setHeading4HorizontalRule(value as Boolean) as Void {
            _heading4HorizontalRule = value;
        }

        function getImageSpacingAfter() as Number {
            return _imageSpacingAfter;
        }

        function setImageSpacingAfter(value as Number) as Void {
            _imageSpacingAfter = value;
        }

        function getListFont() as Number {
            return _listFont;
        }

        function setListFont(value as Number) as Void {
            _listFont = value;
        }

        function getListSpacingAfter() as Number {
            return _listSpacingAfter;
        }

        function setListSpacingAfter(value as Number) as Void {
            _listSpacingAfter = value;
        }

        function getListItemFont() as Number {
            return _listItemFont;
        }

        function setListItemFont(value as Number) as Void {
            _listItemFont = value;
        }

        function getListItemSpacingAfter() as Number {
            return _listItemSpacingAfter;
        }

        function setListItemSpacingAfter(value as Number) as Void {
            _listItemSpacingAfter = value;
        }

        function getParagraphFont() as Number {
            return _paragraphFont;
        }

        function setParagraphFont(value as Number) as Void {
            _paragraphFont = value;
        }

        function getParagraphSpacingAfter() as Number {
            return _paragraphSpacingAfter;
        }

        function setParagraphSpacingAfter(value as Number) as Void {
            _paragraphSpacingAfter = value;
        }

        function getTableSpacingAfter() as Number {
            return _tableSpacingAfter;
        }

        function setTableSpacingAfter(value as Number) as Void {
            _tableSpacingAfter = value;
        }

        function getTableCellPaddingTopBottom() as Number {
            return _tableCellPaddingTopBottom;
        }

        function setTableCellPaddingTopBottom(value as Number) as Void {
            _tableCellPaddingTopBottom = value;
        }

        function getTableCellPaddingSides() as Number {
            return _tableCellPaddingSides;
        }

        function setTableCellPaddingSides(value as Number) as Void {
            _tableCellPaddingSides = value;
        }

        function getTableHeaderBottomLineThickness() as Number {
            return _tableHeaderBottomLineThickness;
        }

        function setTableHeaderBottomLineThickness(value as Number) as Void {
            _tableHeaderBottomLineThickness = value;
        }

        function getMenuTitleHeight() as Number {
            return _menuTitleHeight;
        }

        function setMenuTitleHeight(value as Number) as Void {
            _menuTitleHeight = value;
        }

        function getMenuTitleFont() as Number {
            return _menuTitleFont;
        }

        function setMenuTitleFont(value as Number) as Void {
            _menuTitleFont = value;
        }

        function getMenuItemHeight() as Number {
            return _menuItemHeight;
        }

        function setMenuItemHeight(value as Number) as Void {
            _menuItemHeight = value;
        }

        function getMenuItemFont() as Number {
            return _menuItemFont;
        }

        function setMenuItemFont(value as Number) as Void {
            _menuItemFont = value;
        }

        function getMenuFocusHeight() as Number {
            return _menuFocusHeight;
        }

        function setMenuFocusHeight(value as Number) as Void {
            _menuFocusHeight = value;
        }

        function getMenuFocusFont() as Number {
            return _menuFocusFont;
        }

        function setMenuFocusFont(value as Number) as Void {
            _menuFocusFont = value;
        }

        function equals(object as Theme) as Boolean {
            return
                object instanceof Theme &&
                _backgroundColor == object.getBackgroundColor() &&
                _textColor == object.getTextColor() &&
                _majorAccentColor == object.getMajorAccentColor() &&
                _minorAccentColor == object.getMinorAccentColor() &&
                _heading1Font == object.getHeading1Font() &&
                _heading1SpacingAfter == object.getHeading1SpacingAfter() &&
                _heading1HorizontalRule == object.getHeading1HorizontalRule() &&
                _heading2Font == object.getHeading2Font() &&
                _heading2SpacingAfter == object.getHeading2SpacingAfter() &&
                _heading2HorizontalRule == object.getHeading2HorizontalRule() &&
                _heading3Font == object.getHeading3Font() &&
                _heading3SpacingAfter == object.getHeading3SpacingAfter() &&
                _heading3HorizontalRule == object.getHeading3HorizontalRule() &&
                _heading4Font == object.getHeading4Font() &&
                _heading4SpacingAfter == object.getHeading4SpacingAfter() &&
                _heading4HorizontalRule == object.getHeading4HorizontalRule() &&
                _imageSpacingAfter == object.getImageSpacingAfter() &&
                _listFont == object.getListFont() &&
                _listSpacingAfter == object.getListSpacingAfter() &&
                _listItemFont == object.getListItemFont() &&
                _listItemSpacingAfter == object.getListItemSpacingAfter() &&
                _paragraphFont == object.getParagraphFont() &&
                _paragraphSpacingAfter == object.getParagraphSpacingAfter() &&
                _tableSpacingAfter == object.getTableSpacingAfter() &&
                _tableCellPaddingTopBottom == object.getTableCellPaddingTopBottom() &&
                _tableCellPaddingSides == object.getTableCellPaddingSides() &&
                _tableHeaderBottomLineThickness == object.getTableHeaderBottomLineThickness() &&
                _menuTitleHeight == object.getMenuTitleHeight() &&
                _menuTitleFont == object.getMenuTitleFont() &&
                _menuItemHeight == object.getMenuItemHeight() &&
                _menuItemFont == object.getMenuItemFont() &&
                _menuFocusHeight == object.getMenuFocusHeight() &&
                _menuFocusFont == object.getMenuFocusFont();
        }

        function toString() as String {
            return "Codex.Theme{" +
                "backgroundColor=" + _backgroundColor.toString() + "," +
                "textColor=" + _textColor.toString() + "," +
                "majorAccentColor=" + _majorAccentColor.toString() + "," +
                "minorAccentColor=" + _minorAccentColor.toString() + "," +
                "heading1Font=" + _heading1Font.toString() + "," +
                "heading1SpacingAfter=" + _heading1SpacingAfter.toString() + "," +
                "heading1HorizontalRule=" + _heading1HorizontalRule.toString() + "," +
                "heading2Font=" + _heading2Font.toString() + "," +
                "heading2SpacingAfter=" + _heading2SpacingAfter.toString() + "," +
                "heading2HorizontalRule=" + _heading2HorizontalRule.toString() + "," +
                "heading3Font=" + _heading3Font.toString() + "," +
                "heading3SpacingAfter=" + _heading3SpacingAfter.toString() + "," +
                "heading3HorizontalRule=" + _heading3HorizontalRule.toString() + "," +
                "heading4Font=" + _heading4Font.toString() + "," +
                "heading4SpacingAfter=" + _heading4SpacingAfter.toString() + "," +
                "heading4HorizontalRule=" + _heading4HorizontalRule.toString() + "," +
                "imageSpacingAfter=" + _imageSpacingAfter.toString() + "," +
                "listFont=" + _listFont.toString() + "," +
                "listSpacingAfter=" + _listSpacingAfter.toString() + "," +
                "listItemFont=" + _listItemFont.toString() + "," +
                "listItemSpacingAfter=" + _listItemSpacingAfter.toString() + "," +
                "paragraphFont=" + _paragraphFont.toString() + "," +
                "paragraphSpacingAfter=" + _paragraphSpacingAfter.toString() + "," +
                "tableSpacingAfter=" + _tableSpacingAfter.toString() + "," +
                "tableCellPaddingTopBottom=" + _tableCellPaddingTopBottom.toString() + "," +
                "tableCellPaddingSides=" + _tableCellPaddingSides.toString() + "," +
                "tableHeaderBottomLineThickness=" + _tableHeaderBottomLineThickness.toString() + "," +
                "menuTitleHeight=" + _menuTitleHeight.toString() + "," +
                "menuTitleFont=" + _menuTitleFont.toString() + "," +
                "menuItemHeight=" + _menuItemHeight.toString() + "," +
                "menuItemFont=" + _menuItemFont.toString() + "," +
                "menuFocusHeight=" + _menuFocusHeight.toString() + "," +
                "menuFocusFont=" + _menuFocusFont.toString() + "}";
        }
    }
}
