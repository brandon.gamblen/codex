import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;
import Toybox.System;


module Codex {

    class Image extends Element {

        private static var _resourceSymbolTable = {};

        private var _resourceId as String;
        private var _altText as String;
        private var _spacingAfter as Number;

        private var _bitmapResource as WatchUi.BitmapResource;
        private var _bitmap as WatchUi.Bitmap;
        private var _dims as Array<Number>;

        function initialize(resourceId as String, altText as String, theme as Theme) {
            /*
            A Markdown image element

            :param resourceId: Bitmap resource ID
            :param altText: Alternative text
            :param theme: Styling options
            */
            Element.initialize([]);

            _resourceId = resourceId;
            _altText = altText;
            _spacingAfter = theme.getImageSpacingAfter();

            _bitmap = null;
        }

        function getResourceId() as String {
            return _resourceId;
        }

        function getAltText() as String {
            return _altText;
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {
            // Load bitmap (if not already loaded)
            if (_bitmap == null) {
                var resourceSymbol = _resourceSymbolTable[_resourceId];
                if (resourceSymbol == null) {
                    throw new Lang.InvalidValueException(
                        "String \"" + _resourceId + "\" not present in resource table");
                }
                _bitmapResource = WatchUi.loadResource(resourceSymbol);
                _bitmap = new WatchUi.Bitmap({:bitmap => _bitmapResource});
                _dims = _bitmap.getDimensions();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // TODO: implement alt text when bitmap not loaded
            dc.drawBitmap(x, y, _bitmapResource);
        }

        function unload() as Void {
            // Unload bitmap content
            _bitmapResource = null;
            _bitmap = null;
            _dims = null;
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Bool {
            return _dims[1] >= threshold;
        }

        protected function getHeightImpl() as Number {
            return _dims[1];
        }

        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            // Get point 0 < scollPoint <= y <= height that will be aligned with top edge of screen
            if (y <= 0) {
                return 0;
            } else {
                return y <= height ? y : height;
            }
        }

        function equals(object as Image) as Boolean {
            return 
                object instanceof Image &&
                _resourceId.equals(object.getResourceId()) &&
                _altText.equals(object.getAltText());
        }

        function toString() as String {
            return "Image{" +
                "resourceId=\"" + getResourceId() + "\"," +
                "altText=\"" + getAltText() + "\"}";
        }

        // Garmin... I shouldn't need something like this just to refer to resources
        static function setResourceSymbolTable(resourceSymbolTable as Dictionary) as Void {
            _resourceSymbolTable = resourceSymbolTable;
        }
    }
}
