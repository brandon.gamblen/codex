

module Codex {

    class Heading1 extends TextElement {

        function initialize(str as String, theme as Theme) as Void {
            /*
            Markdown Heading level 1 element

            :param str: Heading text
            :param theme: Styling options
            */
            TextElement.initialize(str, theme.getHeading1Font(), theme.getHeading1SpacingAfter(),
                theme.getHeading1HorizontalRule(), theme);
        }

        function equals(object as Heading1) as Boolean {
            return 
                object instanceof Heading1 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading1{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
