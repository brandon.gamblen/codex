import Toybox.Lang;
import Toybox.Math;
import Toybox.Graphics;
import Toybox.System;

import Iterators;


module Codex {

    class List extends Element {

        enum {
            ENUMTYPE_BULLET,
            ENUMTYPE_ARABIC,
            ENUMTYPE_ROMANLOWER,
            ENUMTYPE_ROMANUPPER,
            ENUMTYPE_ALPHALOWER,
            ENUMTYPE_ALPHAUPPER
        }

        private var _level as Number;
        private var _enumType as Number;
        private var _textColor as Number;
        private var _backgroundColor as Number;
        private var _font as Number;
        private var _spacingAfter as Number;
        private var _listItemSpacingAfter as Number;

        private var _indent as Number;
        private var _labelWidth as Number;
        private var _labelHeight as Number;

        function initialize(items as Array<Element> or Iterators.BaseIterator, level as Number, enumType as Number,
            theme as Theme) {
            /*
            List of elements

            :param items: List elements (paragraphs)
            :param level: List nesting level
            :param enumType: Enumeration method (bullets, arabic numerals, roman numerals, lowercase alphabet, etc.)
            :param theme: Styling options
            */
            Element.initialize(items);

            if (level > 2) {
                throw new Lang.InvalidValueException(
                    "Cannot nest lists more than 2 level(s) deep");
            }

            _level = level;
            _enumType = enumType;
            _textColor = theme.getTextColor();
            _backgroundColor = theme.getBackgroundColor();
            _font = theme.getListFont();
            _spacingAfter = theme.getListSpacingAfter();
            _listItemSpacingAfter = theme.getListItemSpacingAfter();
        }

        function getLevel() as Number {
            return _level;
        }

        function getEnumType() as Number {
            return _enumType;
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {

            self.width = width;

            // TODO: support wider labels and/or line wrapping for labels
            _labelHeight = TextElement.getFontHeight(_font);

            if (_enumType == ENUMTYPE_BULLET) {
                _labelWidth = (_labelHeight * 0.64).toNumber();
            } else {
                _labelWidth = (_labelHeight * 1.32).toNumber();
            }
            _indent = (_labelHeight * 1.32).toNumber();  // Indent amount
            if (_labelWidth >= width || _indent >= width) {
                throw new Lang.InvalidValueException(
                    "List cannot be laid out within provided width");
            }

            var yTop = 0;

            while (yTop < yMax && children.hasNext()) {
                
                var child = children.next();

                if (child.isHeightKnown() && yTop + child.getHeight() < yMin) {
                    // Child is not on screen
                    child.unload();
                } else {
                    // Unknown if child is on screen, load up to screen height
                    if (child instanceof ListItem) {
                        // Case: list item
                        child.load(dc, width - _labelWidth, yMin - yTop, yMax - yTop);
                    } else {
                        // Case: nested list
                        child.load(dc, width - _indent, yMin - yTop, yMax - yTop);
                    }
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been loaded up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (children.hasNext()) {
                    yTop += child.getSpacingAfter();
                } else {
                    // Full height reached, memoize value
                    height = yTop;
                    break;
                }
            }

            // Unload remaining off-screen children without generating new items, i.e. triggering Markdown parsing
            var memo = children.getMemo();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }

            children.reset();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {

            var itemCounter = 1;

            var yTop = y;
            var yMax = dc.getHeight();

            while (yTop < yMax && children.hasNext()) {

                var item = children.next();

                if (!item.isHeightKnown() || yTop + item.getHeight() >= 0) {
                    // Item is on-screen or possibly on-screen

                    if (item instanceof ListItem) {
                        // List item
                        if (yTop + _labelHeight > 0) {
                            // Draw label
                            var label = _label(itemCounter);

                            dc.setColor(_textColor, _backgroundColor);
                            dc.drawText(x, yTop, _font, label, Graphics.TEXT_JUSTIFY_LEFT);
                        }

                        // Draw item
                        item.draw(dc, x + _labelWidth, yTop);
                    } else {
                        // Item is nested list
                        item.draw(dc, x + _indent, yTop);
                    }

                    if (!item.isHeightKnown() && item.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been drawn up to bottom of screen, no need to continue
                        break;
                    }
                }

                yTop += item.getHeight();
                if (children.hasNext()) {
                    yTop += item.getSpacingAfter();
                }

                if (item instanceof ListItem) {
                    itemCounter ++;
                }
            }

            children.reset();
        }

        private function _label(number as Number) as String {
            // Get the nth label for the list
            switch(_enumType) {
                case List.ENUMTYPE_BULLET:
                    switch (_level) {
                        case 1:
                            return "•";
                        case 2:
                            return "•";
                        case 3:
                            return "•";
                        default:
                            throw new Lang.InvalidValueException(
                                "Invalid nesting level: " + _level.toString());
                    }
                case List.ENUMTYPE_ARABIC:
                    return number.toString() + ".";
                case List.ENUMTYPE_ROMANLOWER:
                    return intToRoman(number).toLower() + ".";
                case List.ENUMTYPE_ROMANUPPER:
                    return intToRoman(number) + ".";
                case List.ENUMTYPE_ALPHALOWER:
                    return intToAlpha(number).toLower() + ".";
                case List.ENUMTYPE_ALPHAUPPER:
                    return intToAlpha(number) + ".";
                default:
                    throw new Lang.InvalidValueException(
                        "Invalid enum type: " + _enumType.toString());
            }
        }

        function getSpacingAfter() as Number {
            // No extra spacing after if list is a nested list
            return _level == 0 ? _spacingAfter : _listItemSpacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function equals(object as List) as Boolean {
            return 
                object instanceof List &&
                getChildren().equals(object.getChildren()) &&
                getLevel() == object.getLevel() &&
                getEnumType() == object.getEnumType();
        }

        function toString() as String {
            return "List{" +
                "children=\"" + getChildren().toString() + "\"," +
                "level=" + getLevel().toString() + "," +
                "enumType=" + getEnumType().toString() + "}";
        }

        static function intToRoman(number as Number) as String {
            /*
            Convert a natural number to a Roman numeral

            Adapted from: https://www.oreilly.com/library/view/python-cookbook/0596001673/ch03s24.html

            :param number: Natural number (range: [1, 3999])
            */
            if (number < 1 || number > 3999) {
                throw new Lang.InvalidValueException(
                    "Input out of range: " + number.toString());
            }

            var ROMAN_INTS = [1000, 900, 500, 400,  100, 90,   50,  40,   10,  9,    5,   4,    1  ];
            var ROMAN_NUMS = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

            var result = "";
            for (var i = 0; i < ROMAN_INTS.size(); i++) {
                var int = ROMAN_INTS[i];
                var numeral = ROMAN_NUMS[i];
                var count = (number / int).toNumber();
                for (var j = 0; j < count; j++) {
                    result += numeral;
                }
                number -= int * count;
            }

            return result;
        }

        static function romanToInt(numeral as String) as Number {
            /*
            Convert a Roman numeral to a natural number

            :param numeral: Roman numeral (range: [1 ("I"), 3999 ("MMMCMXCIX")])
            */
            var ROMAN_INTS = [1000, 900, 500, 400,  100, 90,   50,  40,   10,  9,    5,   4,    1  ];
            var ROMAN_NUMS = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

            var result = 0;
            var i = 0;
            while (numeral.length() > 0 && i < ROMAN_INTS.size()) {
                var numGroup = ROMAN_NUMS[i];
                var numGroupLen = numGroup.length();
                if (numGroupLen <= numeral.length() &&
                    numeral.substring(0, numGroupLen).equals(numGroup)) {
                    // Numeral starts with num group
                    result += ROMAN_INTS[i];
                    numeral = numeral.substring(numGroupLen, numeral.length());
                } else {
                    // Move search to next numeral group
                    i++;
                }
            }

            if (numeral.length() > 0) {
                throw new Lang.InvalidValueException(
                    "Invalid or out-of-range input");
            }

            return result;
        }

        static function intToAlpha(number as Number) as String {
            /*
            Convert a natural number to an alphabetical numbering

            :param number: Natural number (range: [1, infinity))
            */
            var result = "";
            while (number > 0) {
                var rem = number % 26;
                result = ('A' + (rem - 1)).toString() + result;
                number = (number - rem) / 26;
            }
            return result;
        }
    }

    (:test)
    function testIntToRoman(logger as Logger) as Boolean {
        var testInput =  [1,   3,     99,     554,    3999       ];
        var testOutput = ["I", "III", "XCIX", "DLIV", "MMMCMXCIX"];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.intToRoman(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }

    (:test)
    function testRomanToInt(logger as Logger) as Boolean {
        var testInput =  ["I", "III", "XCIX", "DLIV", "MMMCMXCIX"];
        var testOutput = [1,   3,     99,     554,    3999       ];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.romanToInt(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }

    (:test)
    function testIntToAlpha(logger as Logger) as Boolean {
        var testInput =  [1,   3,   99,   554,  3999];
        var testOutput = ["A", "C", "CU", "UH", "EWU"];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.intToAlpha(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }
}
