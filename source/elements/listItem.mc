

module Codex {

    class ListItem extends TextElement {

        function initialize(str as String, theme as Theme) {
            /*
            An item in a list

            :param str: String content
            :param theme: Styling options
            */
            TextElement.initialize(str, theme.getListItemFont(), theme.getListItemSpacingAfter(),
                false, theme);
        }

        function equals(object as ListItem) as Boolean {
            return 
                object instanceof ListItem &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "ListItem{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
