import Toybox.Graphics;
import Toybox.Lang;

import Iterators;


module Codex {

    class Table extends Element {

        private var _borderColor as Number;
        private var _backgroundColor as Number;
        private var _spacingAfter as Number;

        function initialize(rows as Array<TableRow> or Iterators.BaseIterator, theme as Theme) {
            /*
            Markdown table element

            :param rows: Table row elements
            :param theme: Styling options
            */
            if ((rows instanceof Array && rows.size() == 0) ||
                (rows instanceof Iterators.BaseIterator && !rows.hasNext())) {
                throw new Lang.InvalidValueException(
                    "Cannot create Table with zero rows");
            }
            Element.initialize(rows);

            _borderColor = theme.getMajorAccentColor();
            _backgroundColor = theme.getBackgroundColor();
            _spacingAfter = theme.getTableSpacingAfter();
        }

        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {

            if (y >= 0 && y < dc.getHeight()) {
                // Draw top line
                dc.setColor(_borderColor, _backgroundColor);
                dc.setPenWidth(1);
                dc.drawLine(x, y, x + width, y);
            }

            // Add one pixel for border thickness
            Element.draw(dc, x, y + 1);
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Bool {
            // Add one pixel for border thickness
            return Element.isHeightGreaterThanOrEqualToImpl(threshold - 1);
        }

        protected function getHeightImpl() as Number {
            // Add one pixel for border thickness
            return Element.getHeightImpl() + 1;
        }

        function equals(object as Element) as Boolean {
            return 
                object instanceof Table &&
                Element.equals(object);
        }

        function toString() as String {
            var result = "Table{rows=[";
            var rows = getChildren();
            while (rows.hasNext()) {
                result += rows.toString() + ",";
            }
            result += "]}";
            return result;
        }
    }
}
