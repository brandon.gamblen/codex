import Iterators;


module Codex {

    class UnorderedList extends List {

        function initialize(items as Array<Element> or Iterators.BaseIterator, level as Number,
            theme as Theme) {
            /*
            An unordered list. Items are enumerated by bullets.

            :param items: An array or iterator of ListItem objects
            :param level: List nesting level
            :param theme: Styling options
            */
            List.initialize(items, level, List.ENUMTYPE_BULLET, theme);
        }

        function equals(object as UnorderedList) as Boolean {
            return 
                object instanceof UnorderedList &&
                List.equals(object);
        }

        function toString() as String {
            return "UnorderedList{" +
                "children=\"" + getChildren().toString() + "\"," +
                "level=" + getLevel().toString() + "}";
        }
    }
}
