

module Codex {

    class TableHeader extends TableCell {

        private var _bottomLineThickness as Number;
        
        function initialize(content as Element, theme as Theme) {
            /*
            Markdown table header element

            :param content: Table header content
            :param theme: Styling options
            */
            TableCell.initialize(content, theme);

            _bottomLineThickness = theme.getTableHeaderBottomLineThickness();
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Bool {
            // Extra pixel for thicker bottom line
            return content.isHeightGreaterThanOrEqualTo(threshold - (_bottomLineThickness + 2 * paddingTopBottom));
        }

        function getHeight() as Number {
            // Extra pixel for thicker bottom line
            return content.getHeight() + (_bottomLineThickness + 2 * paddingTopBottom);
        }

        function equals(object as TableHeader) as Boolean {
            return 
                object instanceof TableHeader &&
                content.equals(object.getContent());
        }

        function toString() as String {
            return "TableHeader{" +
                "content=" + content + "}";
        }
    }
}
