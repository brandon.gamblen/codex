import Toybox.Lang;

import Iterators;


module Codex {

    class TableRow extends Element {

        private var _borderColor as Number;
        private var _backgroundColor as Number;
        private var _headerBottomLineThickness as Number;

        private var _isHeaderRow as Boolean;

        function initialize(cells as Array<TableCell> or Iterators.BaseIterator, theme as Theme) {
            /*
            Markdown table header element

            :param cells: Table cells
            :param theme: Styling options
            */
            if ((cells instanceof Array && cells.size() == 0) ||
                (cells instanceof Iterators.BaseIterator && !cells.hasNext())) {
                throw new Lang.InvalidValueException(
                    "Cannot create TableRow with zero cells");
            }
            Element.initialize(cells);

            _borderColor = theme.getMajorAccentColor();
            _backgroundColor = theme.getBackgroundColor();
            _headerBottomLineThickness = theme.getTableHeaderBottomLineThickness();
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {

            self.width = width;

            var cells = getChildren().asArray();

            _isHeaderRow = cells.size() > 0 && cells[0] instanceof TableHeader;
            var bottomLineThickness = _isHeaderRow ? _headerBottomLineThickness : 1;

            var cellWidth = (width - 1).toFloat() / cells.size();
            height = 1;

            for (var i = 0; i < cells.size(); i++) {
                var cell = cells[i];
                var contentWidth = (Math.round((i + 1) * cellWidth).toNumber() - Math.round(i * cellWidth).toNumber()) - 1;
                cell.load(dc, contentWidth, 0, 0x7FFFFFFF);

                var cellHeight = cell.getHeight();
                height = cellHeight + bottomLineThickness > height ? cellHeight + bottomLineThickness : height;
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            
            var cells = children.asArray();
            var cellWidth = (width - 1).toFloat() / cells.size();

            // Draw cell content
            for (var i = 0; i < cells.size(); i++) {
                cells[i].draw(dc, x + Math.round(i * cellWidth).toNumber() + 1, y);
            }

            // Draw cell borders
            dc.setColor(_borderColor, _backgroundColor);
            dc.setPenWidth(1);
            for (var i = 0; i <= cells.size(); i++) {
                var xLine = x + Math.round(i * cellWidth).toNumber();
                dc.drawLine(xLine, y, xLine, y + height - 1);
            }

            // Draw bottom line
            if (_isHeaderRow) {
                dc.setPenWidth(_headerBottomLineThickness);
                dc.drawLine((x + (_headerBottomLineThickness) / 2).toNumber(), y + height - 1, x + width, y + height - 1);
            } else {
                dc.drawLine(x, y + height - 1, x + width, y + height - 1);
            }
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Bool {
            // Or over cells
            var result = false;
            while (children.hasNext()) {
                var cell = children.next();
                if (cell.isHeightGreaterThanOrEqualTo(threshold)) {
                    result = true;
                    break;
                }
            }
            children.reset();
            return result;
        }

        protected function getHeightImpl() as Number {
            // Maximum over cells
            var result = 0;
            while (children.hasNext()) {
                var cell = children.next();
                var cellHeight = cell.getHeight();
                result = cellHeight > result ? cellHeight : result;
            }
            children.reset();
            return result;
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            var scrollPoint = 0;
            while (children.hasNext()) {
                var cell = children.next();
                var cellScrollPoint = cell.getScrollPoint(y);
                scrollPoint = cellScrollPoint > scrollPoint ? cellScrollPoint : scrollPoint;
            }
            children.reset();
            return scrollPoint;
        }

        function equals(object as TableRow) as Boolean {
            return 
                object instanceof TableRow &&
                Element.equals(object);
        }

        function toString() as String {
            var result = "TableRow{cells=[";
            while (children.hasNext()) {
                var cell = children.next();
                result += cell.toString() + ",";
            }
            children.reset();
            result += "]}";
            return result;
        }
    }
}
