import Iterators;


module Codex {

    class Paragraph extends TextElement {

        function initialize(lines as String or Iterators.BaseIterator, theme as Theme) as Void {
            /*
            Markdown paragraph element

            :param str: Paragraph text. Can be singular string or iterator of strings.
            :param theme: Styling options
            */
            TextElement.initialize(lines, theme.getParagraphFont(), theme.getParagraphSpacingAfter(),
                false, theme);
        }

        function equals(object as Paragraph) as Boolean {
            return 
                object instanceof Paragraph &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Paragraph{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
