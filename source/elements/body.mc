import Iterators;


module Codex {

    class Body extends Element {

        function initialize(children as Array<Element> or Iterators.MemoIterator) {
            Element.initialize(children);
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function equals(object as Body) as Boolean {
            return 
                object instanceof Body &&
                Element.equals(object);
        }

        function toString() as String {
            return "Body{" +
                "children=" + getChildren().toString() + "}";
        }
    }
}
