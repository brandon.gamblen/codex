import Toybox.Graphics;
import Toybox.Math;

import Iterators;
import Strings;


module Codex {

    class TextElement extends Element {

        protected static var fontHeight = {};

        static function computeFontHeights(dc as Graphics.Dc) {
            // Build a static table of font->height
            var fontSet = [
                Graphics.FONT_XTINY,
                Graphics.FONT_TINY,
                Graphics.FONT_SMALL,
                Graphics.FONT_MEDIUM,
                Graphics.FONT_LARGE,
            ];
            for (var i = 0; i < fontSet.size(); i++) {
                var font = fontSet[i];
                fontHeight.put(font, dc.getFontHeight(font));
            }
        }

        static function getFontHeight(font as Number) as Number {
            return fontHeight[font];
        }

        // Amount of spacing before a horizontal rule
        static const HORIZONTAL_RULE_Y_SPACING = 3;

        private var _str as String or Iterators.BaseIterator;
        private var _textColor as Number;
        private var _backgroundColor as Number;
        private var _horizontalRuleColor as Number;
        private var _font as Number;
        private var _spacingAfter as Number;
        private var _horizontalRule as Boolean;

        private var _lines as TextWrapper;

        function initialize(str as String or Iterators.BaseIterator, font as Number, spacingAfter as Number,
            horizontalRule as Boolean, theme as Theme) as Void {
            /*
            Generic text element

            :param str: Text. Can be a singular string or a collection of lines.
            :param font: Font to use
            :param spacingAfter: Amount of spacing after text
            :param horizontalRule: Whether to draw a horizontal rule below text
            :param theme: Styling options
            */
            Element.initialize([]);

            _str = str;
            _textColor = theme.getTextColor();
            _backgroundColor = theme.getBackgroundColor();
            _horizontalRuleColor = theme.getMajorAccentColor();
            _font = font;
            _spacingAfter = spacingAfter;
            _horizontalRule = horizontalRule;

            _lines = null;
        }

        function getStr() as String {
            return _str;
        }

        function getFont() as Number {
            return _font;
        }
        
        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function getHorizontalRule() as Number {
            return _horizontalRule;
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {
            // Load text (if not already loaded)
            if (_lines == null) {
                self.width = width;
                _lines = new Strings.TextWrapper(dc, _str, width, _font);
            }

            // Pre-load lines
            var lineHeight = fontHeight[_font];
            var yTop = 0;
            while (yTop < yMax) {
                _lines.next();
                yTop += lineHeight;
                if (!_lines.hasNext()) {
                    height = yTop;
                    break;
                }
            }
            _lines.reset();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {

            dc.setColor(_textColor, _backgroundColor);

            var lineHeight = fontHeight[_font];

            // Render all lines on-screen
            var yMin = -lineHeight + 1;
            var yMax = dc.getHeight();
            var yTop = y;

            while (yTop < yMax) {
                var line = _lines.next();
                if (line != null) {
                    if (yTop >= yMin) {
                        // Text is at least partially on-screen
                        dc.drawText(x, yTop, _font, line, Graphics.TEXT_JUSTIFY_LEFT);
                    }
                    yTop += lineHeight;
                } else {
                    break;
                }
            }
            _lines.reset();

            if (_horizontalRule) {
                // Render horizontal rule
                var lineX = x;
                var lineY = yTop + HORIZONTAL_RULE_Y_SPACING;

                dc.setColor(_horizontalRuleColor, _backgroundColor);
                dc.setPenWidth(1);
                dc.drawLine(lineX, lineY, lineX + width, lineY);
            }
        }

        function unload() as Void {
            if (_lines != null) {
                _lines.reset();
            }
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Bool {
            // Count at least as many lines as required
            var lineHeight = fontHeight[_font];
            var thresholdNumLines = Math.ceil(threshold / lineHeight).toNumber();
            var numLines = 0;
            while (numLines < thresholdNumLines && _lines.next() != null) {
                numLines ++;
            }
            if (!_lines.hasNext()) {
                // Lines are exhausted, height is known
                height = numLines * lineHeight;
            }
            _lines.reset();

            return numLines >= thresholdNumLines;
        }

        protected function getHeightImpl() as Number {
            // Need to load all lines to determine height
            var numLines = _lines.asArray().size();
            var lineHeight = fontHeight[_font];
            return numLines * lineHeight;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            // Get y of most recent non-empty line
            var yTop = 0;
            var scrollPoint = 0;
            var lineHeight = fontHeight[_font];

            while (yTop <= y) {
                var line = _lines.next();
                if (line != null) {
                    if (!line.equals("")) {
                        scrollPoint = yTop;
                    }
                    yTop += lineHeight;
                } else {
                    break;
                }
            }
            _lines.reset();

            return scrollPoint;
        }

        function equals(object as TextElement) as Boolean {
            return 
                object instanceof TextElement &&
                object.getStr().equals(_str) &&
                object.getFont() == _font;
        }
    }
}
