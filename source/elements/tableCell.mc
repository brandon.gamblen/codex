

module Codex {

    class TableCell extends Element {

        protected var paddingTopBottom as Number;
        protected var paddingSides as Number;

        protected var content as Element;

        function initialize(content as Element, theme as Theme) {
            /*
            Markdown table cell element

            :param content: Table cell content
            :param theme: Styling options
            */
            Element.initialize([]);

            self.content = content;

            paddingTopBottom = theme.getTableCellPaddingTopBottom();
            paddingSides = theme.getTableCellPaddingSides();
        }

        function getContent() as Element {
            return content;
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {
            content.load(dc, width - paddingSides * 2, yMin, yMax - 2 * paddingTopBottom);
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            content.draw(dc, x + paddingSides, y + paddingTopBottom);
        }

        function unload() as Void {
            content.unload();
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Bool {
            return content.isHeightGreaterThanOrEqualTo(threshold - paddingTopBottom * 2);
        }

        function getHeight() as Number {
            return content.getHeight() + paddingTopBottom * 2;
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return content.hasScrollPoints();
        }

        function getScrollPoint(y as Number) as Number {
            return content.getScrollPoint(y - paddingTopBottom) + paddingTopBottom;
        }

        function equals(object as TableCell) as Boolean {
            return 
                object instanceof TableCell &&
                content.equals(object.getContent());
        }

        function toString() as String {
            return "TableCell{" +
                "content=" + content + "}";
        }
    }
}
