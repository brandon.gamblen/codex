import Toybox.Graphics;
import Toybox.System;
import Toybox.Lang;

import Iterators;


module Codex {

    class Element {

        protected var children as Iterators.BaseIterator;
        protected var width as Number;
        protected var height as Number;

        function initialize(children as Array<Element> or Iterators.BaseIterator) {
            /*
            Base class for Markdown elements

            :param children: Child elements
            */
            if (children instanceof Array) {
                children = new Iterators.ArrayIterator(children);
            }
            self.children = children;
        }

        function getChildren() as Iterators.BaseIterator {
            return children;
        }

        function load(dc as Graphics.Dc, width as Number, yMin as Number, yMax as Number) as Void {
            /*
            Load content

            :param dc: Display context
            :param width: Content width
            :param yMin: Start of content range
            :param yMax: End of content range
            */
            self.width = width;
            var yTop = 0;

            while (yTop < yMax && children.hasNext()) {
                
                var child = children.next();

                if (child.isHeightKnown() && yTop + child.getHeight() < yMin) {
                    // Child is not on screen
                    child.unload();
                } else {
                    // Unknown if child is on screen, load up to screen height
                    child.load(dc, width, yMin - yTop, yMax - yTop);
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been loaded up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (children.hasNext()) {
                    yTop += child.getSpacingAfter();
                } else {
                    // Full height reached, memoize value
                    height = yTop;
                    break;
                }
            }

            // Unload remaining off-screen children without generating new items, i.e. triggering Markdown parsing
            var memo = children.getMemo();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }

            children.reset();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            /*
            Draw element content

            :param dc: Display context
            :param x: X coordinate to draw at
            :param y: Y coordinate to draw at
            */
            var yTop = y;
            var yMax = dc.getHeight();

            while (yTop < yMax && children.hasNext()) {

                var child = children.next();

                if (!child.isHeightKnown() || yTop + child.getHeight() >= 0) {
                    // Child is on-screen or possibly on-screen
                    child.draw(dc, x, yTop);
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been drawn up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (children.hasNext()) {
                    yTop += child.getSpacingAfter();
                }
            }

            children.reset();
        }

        function unload() as Void {
            // Only unload children that have been seen so far, i.e. don't trigger more Markdown parsing
            var memo = children instanceof Iterators.MemoIterator ? children.getMemo() : children.asArray();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Bool {
            // Check if height of element is greater than or equal to value
            if (height != null) {
                // Full height has already been calculated
                return height >= threshold;
            } else {
                // Use partial height calculation method
                return isHeightGreaterThanOrEqualToImpl(threshold);
            }
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Bool {
            /*
            Test if height is greater than or equal to a threshold. Used to determine if next element should be
            displayed without needing to load full height of this element.

            Element will be loaded for y in [0, threshold] first.
            */
            var totalHeight = 0;
            var result = false;

            while (children.hasNext()) {

                var child = children.next();

                if (child.isHeightGreaterThanOrEqualTo(threshold - totalHeight)) {
                    // Threshold reached, break
                    result = true;
                    if (!child.isHeightKnown()) {
                        // Don't load more content by calling getHeight()
                        break;
                    }
                }

                // Accumulate value
                totalHeight += child.getHeight();
                if (children.hasNext()) {
                    totalHeight += child.getSpacingAfter();
                    if (totalHeight >= threshold) {
                        result = true;
                    }
                } else {
                    // Full height reached, memoize value
                    height = totalHeight;
                }

                if (result == true) {
                    break;
                }
            }

            children.reset();

            return result;
        }

        function getHeight() as Number {
            // Get display height of element
            if (height == null) {
                height = getHeightImpl();
            }
            return height;
        }

        protected function getHeightImpl() as Number {
            /*
            Calculate display height of element.

            Element will be loaded for y in [0, infinity) first.
            */
            var totalHeight = 0;
            while (children.hasNext()) {
                var child = children.next();
                totalHeight += child.getHeight();
                if (children.hasNext()) {
                    totalHeight += child.getSpacingAfter();
                }
            }
            children.reset();
            return totalHeight;
        }

        function isHeightKnown() as Number {
            // Check if height has already been calculated
            return height != null;
        }

        function getSpacingAfter() as Number {
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }

        function hasScrollPoints() as Boolean {
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }

        function getScrollPoint(y as Number) as Number {
            // Get point 0 < scollPoint <= y <= height that will be aligned with top edge of screen
            var yTop = 0;
            var child = null;
            while (children.hasNext()) {
                child = children.next();
                if (child.isHeightGreaterThanOrEqualTo(y - yTop - child.getSpacingAfter())) {
                    // Scroll point is within child or space after child
                    break;
                }
                yTop += child.getHeight();
                if (children.hasNext()) {
                    yTop += child.getSpacingAfter();
                }
            }
            children.reset();
            return yTop + (child != null ? child.getScrollPoint(y - yTop) : 0);
        }

        function equals(object as Element) as Boolean {
            return 
                object instanceof Element &&
                children.equals(object.getChildren());
        }

        function toString() as String {
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }
    }
}