import Toybox.Lang;

import Iterators;


module Codex {

    class OrderedList extends List {

        function initialize(items as Array<Element> or Iterators.BaseIterator, level as Number, enumType as Number,
            theme as Theme) {
            /*
            An ordered list. Items are enumerated according to enumType.

            :param items: An array or iterator of ListItem objects
            :param level: List nesting level
            :param enumType: Enumeration method (arabic numerals, roman numerals, lowercase alphabet, etc.)
            :param theme: Styling options
            */
            if (enumType == List.ENUMTYPE_BULLET) {
                throw new Lang.InvalidValueException(
                    "OrderedList cannot be enumerated by bullets");
            }
            List.initialize(items, level, enumType, theme);
        }

        function equals(object as OrderedList) as Boolean {
            return
                object instanceof OrderedList &&
                List.equals(object);
        }

        function toString() as String {
            return "OrderedList{" +
                "children=\"" + getChildren().toString() + "\"," +
                "level=" + getLevel().toString() + "," +
                "enumType=" + getEnumType().toString() + "}";
        }
    }
}
