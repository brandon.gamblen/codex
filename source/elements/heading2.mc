

module Codex {

    class Heading2 extends TextElement {

        function initialize(str as String, theme as Theme) as Void {
            /*
            Markdown Heading level 2 element

            :param str: Heading text
            :param theme: Styling options
            */
            TextElement.initialize(str, theme.getHeading2Font(), theme.getHeading2SpacingAfter(),
                theme.getHeading2HorizontalRule(), theme);
        }

        function equals(object as Heading2) as Boolean {
            return 
                object instanceof Heading2 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading2{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
