

module Codex {

    class Heading3 extends TextElement {

        function initialize(str as String, theme as Theme) as Void {
            /*
            Markdown Heading level 3 element

            :param str: Heading text
            :param theme: Styling options
            */
            TextElement.initialize(str, theme.getHeading3Font(), theme.getHeading3SpacingAfter(),
                theme.getHeading3HorizontalRule(), theme);
        }

        function equals(object as Heading3) as Boolean {
            return 
                object instanceof Heading3 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading3{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
