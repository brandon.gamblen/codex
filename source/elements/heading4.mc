

module Codex {

    class Heading4 extends TextElement {

        function initialize(str as String, theme as Theme) as Void {
            /*
            Markdown Heading level 4 element

            :param str: Heading text
            :param theme: Styling options
            */
            TextElement.initialize(str, theme.getHeading4Font(), theme.getHeading4SpacingAfter(),
                theme.getHeading4HorizontalRule(), theme);
        }

        function equals(object as Heading4) as Boolean {
            return 
                object instanceof Heading4 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading4{" +
                "str=\"" + getStr() + "\"}";
        }
    }
}
