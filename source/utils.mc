

module Codex {

    class Utils {

        static function getDefault(dict as Dictionary, key as Object, defaultValue as Object?) as Object? {
            /*
            Get a value from a dictionary, returning a default value if the key does not exist
            */
            var value = dict[key];
            if (value == null) {
                value = defaultValue;
            }
            return value;
        }
    }
}