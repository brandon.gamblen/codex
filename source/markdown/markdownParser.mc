import Toybox.Test;
import Toybox.System;
import Toybox.Lang;
import Toybox.Math;

import Iterators;
import Strings;


module Codex {

    class MarkdownParser extends Iterators.MemoIterator {

        static enum {
            STATE_SOF,
            STATE_PARAGRAPH,
            STATE_HEADING,
            STATE_LIST,
            STATE_IMAGE,
            STATE_TABLE,
            STATE_WHITESPACE,
            STATE_EOF,
        }

        // Non-state variables
        private var _theme as Theme;

        // State variables
        private var _preprocessor as MarkdownPreprocessor;
        private var _state as Number;
        private var _subParser as Iterators.BaseIterator?;

        function initialize(str as String or Symbol, theme as Theme) {
            /*
            An iterator for parsing markdown into a sequence of elements

            :param str: Markdown string or resource ID
            :param theme: Styling options
            */
            MemoIterator.initialize();

            _theme = theme;

            _preprocessor = new MarkdownPreprocessor(str);

            _state = STATE_SOF;
            _subParser = null;
        }

        protected function nextImpl() as Element? {
            
            while (_state != STATE_EOF) {

                if (_subParser != null) {
                    // Must capture all lines within sub-parser before moving on to next element
                    _subParser.exploit();
                    _subParser = null;
                }

                var nextLine = _preprocessor.peek();
                var nextState = getState(nextLine);

                var result = null;
                switch(nextState) {
                    case STATE_WHITESPACE:
                        // No element present
                        _preprocessor.next();
                        break;
                    case STATE_HEADING:
                        // Headings always break on newline
                        result = parseHeading(_preprocessor.next(), _theme);
                        break;
                    case STATE_PARAGRAPH:
                        // Paragraphs break when a new element encountered
                        _subParser = _buildSubParser(STATE_PARAGRAPH);
                        result = parseParagraph(_subParser, _theme);
                        break;
                    case STATE_LIST:
                        // Lists break when a new element encountered
                        _subParser = _buildSubParser(STATE_LIST);
                        result = parseList(_subParser, _theme);
                        break;
                    case STATE_IMAGE:
                        // Images are always one line
                        result = parseImage(_preprocessor.next(), _theme);
                        break;
                    case STATE_TABLE:
                        // Tables break when a new element encountered
                        _subParser = _buildSubParser(STATE_TABLE);
                        result = parseTable(_subParser, _theme);
                        break;
                    case STATE_EOF:
                        // End of file, do nothing
                        _preprocessor.next();
                        break;
                    default:
                        throw new Lang.InvalidValueException(
                            "Unhandled case: state=" + state.toString() + 
                            ", nextState=" + nextState.toString());
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        function unload() as Void {
            // Unload string resources
            _preprocessor.unload();
        }

        static function getState(line as String) as Number {
            // Get state from line which will be used to determine how to parse line
            if (line == null) {
                return STATE_EOF;
            }

            var chars = line.toCharArray();
            if (chars.size() == 0) {
                return STATE_WHITESPACE;
            }
            if (chars[0] == '#') {
                return STATE_HEADING;
            }
            if (chars[0] == '|') {
                return STATE_TABLE;
            } 
            if (chars[0] == '!') {
                return STATE_IMAGE;
            }  

            var unindented = Strings.replaceAll("\t", "", line);
            var unindentedChars = unindented.toCharArray();
            if ((unindentedChars[0] == '-' || unindentedChars[0] == '+' || unindentedChars[0] == '*') &&
                unindentedChars[1] == ' ') {
                return STATE_LIST;
            }

            var dotIndex = unindented.find(".");
            if (dotIndex != null && unindented.length() >= dotIndex + 4 &&
                Strings.isAlphanumeric(unindented.substring(0, dotIndex)) &&
                unindented.substring(dotIndex + 1, dotIndex + 3).equals("  ")) {
                return STATE_LIST;
            }

            return STATE_PARAGRAPH;
        }

        private static function _buildSubParser(state as Number) as Iterators.BaseIterator {
            // Chain iterators to return subset of lines corresponding to a state
            var remainingLines = new Iterators.Range(_preprocessor, _preprocessor.getSeek(), null);
            return new Iterators.While(remainingLines, new _StateEquals(state));
        }

        static function parseHeading(line as String, theme as Theme) as TextElement {
            // Parse heading line

            var chars = line.toCharArray();
            var headingLevel = 0;
            var state = :startOfLine;
            var exception = false;

            for (var i = 0; i < chars.size() && state != :nonWhitespace; i++) {
                var char = chars[i];
                var nextState = 
                    char == '#' ? :hashtag :
                    char == ' ' ? :whitespace :
                    char == '\t' ? :tab :
                    :nonWhitespace;

                switch (nextState) {
                    case :hashtag:
                        if (!(state == :startOfLine || state == :hashtag)) {
                            exception = true;
                        }
                        headingLevel ++;
                        break;
                    case :whitespace:
                        if (state != :hashtag) {
                            exception = true;
                        }
                        break;
                    case :nonWhitespace:
                        if (state != :whitespace) {
                            exception = true;
                        }
                        break;
                    case :tab:
                        exception = true;
                        break;
                }

                if (exception) {
                    break;
                }

                state = nextState;
            }

            if (exception || headingLevel == 0 || state != :nonWhitespace) {
                throw new Lang.InvalidValueException(
                    "Invalid heading definition: \"" + line + "\"");
            }

            line = line.substring(headingLevel + 1, line.length());

            switch (headingLevel) {
                case 1:
                    return new Heading1(line, theme);
                case 2:
                    return new Heading2(line, theme);
                case 3:
                    return new Heading3(line, theme);
                case 4:
                    return new Heading4(line, theme);
                default:
                    throw new Lang.InvalidValueException(
                        "Invalid or unsupported heading level: " + headingLevel.toString());
            }
        }

        static function parseImage(line as String, theme as Theme) as Image {

            var altText = null as String?;
            var resourceId = null as String?;

            var state = :startOfLine;
            var lineRemaining = line.toString();  // Copy string

            while (state != :endOfLine && lineRemaining.length() > 0) {

                var trimIndex as Number;

                switch (state) {
                    case :startOfLine:
                        if (!lineRemaining.substring(0, 1).equals("!")) {
                            throw new Lang.InvalidValueException(
                                "Image specifier must start with '!'");
                        }
                        trimIndex = 1;
                        state = :altText;
                        break;
                    case :altText:
                        if (!lineRemaining.substring(0, 1).equals("[")) {
                            throw new Lang.InvalidValueException(
                                "Brackets '[]' must follow '!' in image specifier");
                        }
                        var rightBracketIndex = lineRemaining.find("]");
                        if (rightBracketIndex == null) {
                            throw new Lang.InvalidValueException(
                                "Left bracket '[' not followed by right bracket ']' in image specifier");
                        }
                        altText = lineRemaining.substring(1, rightBracketIndex);
                        trimIndex = rightBracketIndex + 1;
                        state = :resourceId;
                        break;
                    case :resourceId:
                        if (!lineRemaining.substring(0, 1).equals("(")) {
                            throw new Lang.InvalidValueException(
                                "Parentheses '()' must follow brackets '[]' in image specifier");
                        }
                        var rightParenIndex = lineRemaining.find(")");
                        if (rightParenIndex == null) {
                            throw new Lang.InvalidValueException(
                                "Left parenthesis '(' must be followed by right parenthesis ')' in image specifier");
                        }
                        resourceId = lineRemaining.substring(1, rightParenIndex);
                        trimIndex = rightParenIndex + 1;
                        state = :endOfLine;
                        break;
                }

                lineRemaining = lineRemaining.substring(trimIndex, lineRemaining.length());
            }

            if (state != :endOfLine || !lineRemaining.equals("")) {
                throw new Lang.InvalidValueException(
                    "Invalid image specifier: \"" + line + "\"");
            }

            return new Image(resourceId, altText, theme);
        }

        static function parseList(lines as Iterators.BaseIterator, theme as Theme) as List {
            var enumType = MarkdownListParser.getListEnumType(lines.peek(), 0);
            if (enumType == List.ENUMTYPE_BULLET) {
                return new UnorderedList(new MarkdownListParser(lines, 0, List.ENUMTYPE_BULLET, theme), 0, theme);
            } else {
                return new OrderedList(new MarkdownListParser(lines, 0, enumType, theme), 0, enumType, theme);
            }
        }

        static function parseParagraph(lines as String or Iterators.BaseIterator, theme as Theme
            ) as Paragraph {
            return new Paragraph(lines, theme);
        }

        static function parseTable(lines as Iterators.BaseIterator, theme as Theme) as Table {
            return new Table(new MarkdownTableParser(lines, theme), theme);
        }

        class _StateEquals extends Iterators.Condition {

            private var _state as Number;

            function initialize(state as Number) {
                // A condition for capturing a range of lines that all generate the same Markdown state
                Iterators.Condition.initialize();
                _state = state;
            }

            function evaluate(line as String) as Boolean {
                return getState(line) == _state;
            }

            function toString() as String {
                return "_StateEquals{" +
                    "state=" + _state.toString() + "}";
            }
        }
    }


    (:test)
    function testMarkdownParserHelloWorld(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;
        var testStr = "## Message\n### Hello\nHello, World!\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, theme);
        return obj.testAgainstOutput([
            new Heading2("Message", theme),
            new Heading3("Hello", theme),
            new Paragraph(new Iterators.ArrayIterator(["Hello, World!"]), theme),
        ]);
    }


    (:test)
    function testMarkdownParserList(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;
        var testStr = "## My list\n1.  Take out the trash\n2.  ...\n3.  Profit.\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, theme);
        return obj.testAgainstOutput([
            new Heading2("My list", theme),
            new OrderedList(
                new Iterators.ArrayIterator([
                    new ListItem("Take out the trash", theme),
                    new ListItem("...", theme),
                    new ListItem("Profit.", theme),
                ]),
                0,
                List.ENUMTYPE_ARABIC,
                theme
            ),
        ]);
    }


    (:test)
    function testMarkdownParserListRomanNumerals(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;
        var testStr = "## MEUMALBUM\nI.  EXIMITOQUISQUILIAS\nII.  ...\nIII.  PROFICIO\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, theme);
        return obj.testAgainstOutput([
            new Heading2("MEUMALBUM", theme),
            new OrderedList(
                new Iterators.ArrayIterator([
                    new ListItem("EXIMITOQUISQUILIAS", theme),
                    new ListItem("...", theme),
                    new ListItem("PROFICIO", theme),
                ]),
                0,
                List.ENUMTYPE_ROMANUPPER,
                theme
            ),
        ]);
    }


    (:test)
    function testMarkdownParserSublists(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;

        var testStr as String;
        var obj as MarkdownParser;
        var expectedOutput as List;

        // Top-level list: unordered
        testStr = "- Item\n    + Sub-item\n        * Sub-sub-item\n";
        System.println("Test string: \"" + testStr + "\"");
        expectedOutput = new UnorderedList([
            new ListItem("Item", theme),
            new UnorderedList([
                new ListItem("Sub-item", theme),
                new UnorderedList([
                    new ListItem("Sub-sub-item", theme),
                ], 2, theme),
            ], 1, theme),
        ], 0, theme);
        obj = new MarkdownParser(testStr, theme);
        if (!obj.testAgainstOutput([expectedOutput])) {
            return false;
        }

        // Top-level list: ordered
        testStr = "1.  Item\n    a.  Sub-item\n    b.  Sub-item\n        i.  Sub-sub-item\n        ii.  Sub-sub-item\n    c.  Sub-item\n2.  Item\n    - Sub-item\n    - Sub-item\n";
        System.println("Test string: \"" + testStr + "\"");
        expectedOutput = new OrderedList([
            new ListItem("Item", theme),
            new OrderedList([
                new ListItem("Sub-item", theme),
                new ListItem("Sub-item", theme),
                new OrderedList([
                    new ListItem("Sub-sub-item", theme),
                    new ListItem("Sub-sub-item", theme),
                ], 2, List.ENUMTYPE_ROMANLOWER, theme),
                new ListItem("Sub-item", theme),
            ], 1, List.ENUMTYPE_ALPHALOWER, theme),
            new ListItem("Item", theme),
            new UnorderedList([
                new ListItem("Sub-item", theme),
                new ListItem("Sub-item", theme),
            ], 1, theme),
        ], 0, List.ENUMTYPE_ARABIC, theme);
        obj = new MarkdownParser(testStr, theme);
        if (!obj.testAgainstOutput([expectedOutput])) {
            return false;
        }

        return true;
    }

    (:test)
    function testMarkdownParserImage(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;
        var testStr = "![Never gonna give you up...](images/rick_roll.png)\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, theme);
        return obj.testAgainstOutput([
            new Image("images/rick_roll.png", "Never gonna give you up...", theme),
        ]);
    }


    (:test)
    function testMarkdownParserListAfterParagraph(logger as Logger) as Boolean {
        var theme = App.THEME_LIGHT;
        var testStr = "Paragraph\nA.  List item 1\nB.  List item 2";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, theme);

        var para = obj.next();
        Test.assert(para instanceof Paragraph);

        // Simulate a load
        para.getStr().exploit();

        var list = obj.next();
        Test.assert(list instanceof OrderedList);

        Test.assert(!obj.hasNext());

        var listItems = list.getChildren();
        Test.assert(listItems.next() instanceof ListItem);
        Test.assert(listItems.next() instanceof ListItem);
        Test.assert(!listItems.hasNext());

        return true;
    }
}
