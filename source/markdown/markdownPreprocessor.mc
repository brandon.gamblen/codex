import Iterators;
import Strings;


module Codex {

    class MarkdownPreprocessor extends Iterators.MemoIterator {

        private var _lines as Iterators.BaseIterator;

        function initialize(str as String or Symbol) {
            /*
            Markdown pre-processor. Separates string into lines and normalizes whitespace in each line.

            :param str: Markdown string or resource ID
            */
            MemoIterator.initialize();
            
            _lines = new Strings.Splitter(str, "\n");
        }

        protected function nextImpl() as Object? {
            if (_lines.hasNext()) {
                return _normalizeWhitespace(_lines.next());
            } else {
                return null;
            }
        }

        function unload() as Void {
            // Unload string resources
            _lines.unload();
        }

        private static function _normalizeWhitespace(line as String?) as String? {
            // Pre-processing method
            // Removes trailing whitespace and groups spaces into tabs in leading whitespace

            // Replace tabs with 4 spaces
            line = Strings.replaceAll("\t", "    ", line);

            // Remove leading and trailing whitespace
            var unindented = Strings.removeLeadingWhitespace(line, false);
            var numLeadingWhitespace = line.length() - unindented.length();
            line = Strings.removeTrailingWhitespace(unindented, false);

            // Add in tabs as leading whitespace
            var numTabs = (numLeadingWhitespace / 4).toNumber();
            for (var i = 0; i < numTabs; i++) {
                line = '\t' + line;
            }

            return line;
        }
    }
}
