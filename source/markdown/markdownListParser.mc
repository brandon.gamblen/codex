import Toybox.Lang;

import Iterators;


module Codex {

    class MarkdownListParser extends Iterators.MemoIterator {

        static enum {
            STATE_SOF,
            STATE_LISTITEM,
            STATE_NESTED_LIST,
            STATE_EOF,
        }

        private var _lines as Iterators.BaseIterator;
        private var _level as Number;
        private var _enumType as Number;
        private var _theme as Theme;

        private var _state as Number;
        private var _subParser as Iterators.BaseIterator?;

        function initialize(lines as Iterators.BaseIterator, level as Number, enumType as Number,
            theme as Theme) {
            /*
            An iterator for parsing a markdown list into a series of list elements
            */
            MemoIterator.initialize();

            if (level > 2) {
                throw new Lang.InvalidValueException(
                    "Cannot nest lists more than 2 level(s) deep");
            }

            _lines = lines;
            _level = level;
            _enumType = enumType;
            _theme = theme;

            _state = STATE_SOF;
            _subParser = null;
        }

        protected function nextImpl() as Element? {

            while (_state != STATE_EOF) {
                    
                if (_subParser != null) {
                    // Must capture all lines within sub-parser before moving on to next element
                    _subParser.exploit();
                    _subParser = null;
                }

                var nextLine = _lines.peek();
                var nextState = getState(nextLine, _level, _enumType);

                var result = null;
                switch(nextState) {
                    case STATE_LISTITEM:
                        result = new ListItem(_stripEnum(_lines.next()), _theme);
                        break;
                    case STATE_NESTED_LIST:
                        if (_state == STATE_SOF) {
                            throw new Lang.InvalidValueException(
                                "First item in list is over-indented");
                        }
                        _subParser = _buildNestedListSubParser();
                        result = _parseNestedList(_subParser);
                        break;
                    case STATE_EOF:
                        // End of file, do nothing
                        _lines.next();
                        break;
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        static function getState(line as String, level as Number, enumType as Number) as Number {
            // Get state from line which will be used to determine how to parse line
            if (line == null) {
                return STATE_EOF;
            }

            // Strip leading tabs
            line = line.substring(level, line.length());

            var chars = line.toCharArray();

            if (chars[0] == '\t') {
                return STATE_NESTED_LIST;
            } else if (enumType == List.ENUMTYPE_BULLET) {
                // Case: unordered list
                if ((chars[0] == '-' || chars[0] == '+' || chars[0] == '*') &&
                    chars[1] == ' ') {
                    return STATE_LISTITEM;
                } else {
                    throw new Lang.InvalidValueException(
                        "Unordered list item cannot start with '" + chars[0] + "'");
                }
            } else {
                // Case: ordered list
                var dotIndex = line.find(".");
                if (dotIndex != null && line.length() >= dotIndex + 4 &&
                    Strings.isAlphanumeric(line.substring(0, dotIndex)) &&
                    line.substring(dotIndex + 1, dotIndex + 3).equals("  ")) {
                    return STATE_LISTITEM;
                } else {
                    throw new Lang.InvalidValueException(
                        "Invalid ordered list item: \"" + line + "\"");
                }
            }
        }

        private static function _buildNestedListSubParser() as Iterators.BaseIterator {
            // Chain iterators to return subset of lines corresponding to a state
            var remainingLines = new Iterators.Range(_lines, _lines.getSeek(), null);
            return new Iterators.While(remainingLines, new _IsIndented(_level + 1));
        }

        private function _stripEnum(line as String) as String {
            // String enumeration characters from beginning of list item

            // Strip tabs
            line = line.substring(_level, line.length());

            var result as String;

            if (_enumType == List.ENUMTYPE_BULLET) {
                // List is unodered
                if (line.length() < 3) {
                    throw new Lang.InvalidValueException(
                        "Line in bulleted list must be at least 3 characters long");
                }
                var firstChar = line.substring(0, 1).toCharArray()[0];
                if (!(firstChar == '*' || firstChar == '+' || firstChar == '-')) {
                    throw new Lang.InvalidValueException(
                        "Unordered list item cannot start with '" + firstChar + "'");
                }
                if (!(line.substring(1, 2).equals(" ") && !line.substring(2, 3).equals(" "))) {
                    throw new Lang.InvalidValueException(
                        "Bullet must be followed by a single space");
                }

                result = line.substring(2, line.length());

            } else {
                // List is ordered

                // Seperate enumeration group from list item
                if (line.length() < 5) {
                    throw new Lang.InvalidValueException(
                        "Line in ordered list must be at least 5 characters long");
                }
                var dotIndex = line.find(".  ");
                if (dotIndex == -1 || line.length() < dotIndex + 4 ||
                    line.substring(dotIndex + 3, dotIndex + 4).equals(" ")) {
                    throw new Lang.InvalidValueException(
                        "Ordered list labels must be followed by \".  \" (a period and two spaces)");
                }

                var label =  line.substring(0, dotIndex);
                var chars = label.toCharArray();

                // Parse enumeration group
                switch(_enumType) {
                    case List.ENUMTYPE_ARABIC:
                        if (label.toNumber() == null) {
                            throw new Lang.InvalidValueException(
                                "Invalid arabic numeral: \"" + label + "\"");
                        }
                        break;
                    case List.ENUMTYPE_ROMANLOWER:
                        if (!label.equals(label.toLower())) {
                            throw new Lang.InvalidValueException(
                                "Invalid lowercase roman numeral: \"" + label + "\"");
                        }
                        // Convert to int to raise parsing errors, if any
                        List.romanToInt(label.toUpper());
                        break;
                    case List.ENUMTYPE_ROMANUPPER:
                        if (!label.equals(label.toUpper())) {
                            throw new Lang.InvalidValueException(
                                "Invalid uppercase roman numeral: \"" + label + "\"");
                        }
                        // Convert to int to raise parsing errors, if any
                        List.romanToInt(label);
                        break;
                    case List.ENUMTYPE_ALPHALOWER:
                        if (!label.equals(label.toLower())) {
                            throw new Lang.InvalidValueException(
                                "Invalid lowercase alphabetical numeral: \"" + label + "\"");
                        }
                        for (var i = 0; i < chars.size(); i++) {
                            if (!(chars[i] >= 'a' && chars[i] <= 'z')) {
                                throw new Lang.InvalidValueException(
                                    "Invalid lowercase alphabetical numeral: \"" + label + "\"");
                            }
                        }
                        break;
                    case List.ENUMTYPE_ALPHAUPPER:
                        if (!label.equals(label.toUpper())) {
                            throw new Lang.InvalidValueException(
                                "Invalid uppercase alphabetical numeral: \"" + label + "\"");
                        }
                        for (var i = 0; i < chars.size(); i++) {
                            if (!(chars[i] >= 'A' && chars[i] <= 'Z')) {
                                throw new Lang.InvalidValueException(
                                    "Invalid uppercase alphabetical numeral: \"" + label + "\"");
                            }
                        }
                        break;
                    default:
                        throw new Lang.InvalidValueException(
                            "Invalid list enumeration type: " + _enumType.toString());

                } 

                result = line.substring(dotIndex + 3, line.length());
            }
            return result;
        }

        private static function _parseNestedList(lines as Iterators.BaseIterator) as List {
            var enumType = getListEnumType(lines.peek(), _level + 1);
            if (enumType == List.ENUMTYPE_BULLET) {
                return new UnorderedList(
                    new MarkdownListParser(lines, _level + 1, List.ENUMTYPE_BULLET, _theme),
                    _level + 1, _theme);
            } else {
                return new OrderedList(
                    new MarkdownListParser(lines, _level + 1, enumType, _theme),
                    _level + 1, enumType, _theme);
            }
        }

        static function getListEnumType(line as String, level as Number) {
            // Detect the enumeration type of an ordered list based on the first char of the first line

            var chars = line.toCharArray();

            // Check indentation
            // Pre-processor will convert leading spaces to tabs
            for (var i = 0; i < level; i++) {
                if (chars[i] != '\t') {
                    throw new Lang.InvalidValueException(
                        "List item of nesting level " + level.toString() + 
                        " must begin with " + level.toString() + " tab character(s)");
                }
            }
    
            // Check first character of label
            var firstChar = chars[level];
            switch (firstChar) {
                case '-':
                case '+':
                case '*':
                    return List.ENUMTYPE_BULLET;
                case '1':
                    return List.ENUMTYPE_ARABIC;
                case 'i':
                    return List.ENUMTYPE_ROMANLOWER;
                case 'I':
                    return List.ENUMTYPE_ROMANUPPER;
                case 'a':
                    return List.ENUMTYPE_ALPHALOWER;
                case 'A':
                    return List.ENUMTYPE_ALPHAUPPER;
                case '\t':
                    throw new Lang.InvalidValueException(
                        "List item over-indented");
                default:
                    var dotIndex = line.find(".");
                    if (dotIndex != null) {
                        throw new Lang.InvalidValueException(
                            "Invalid list label: cannot start an ordered list with \"" + 
                            line.substring(0, dotIndex) + "\"");
                    } else {
                        throw new Lang.InvalidValueException(
                            "Line contains invalid list label: \"" + line + "\"");
                    }
            }
        }

        class _IsIndented extends Iterators.Condition {

            private var _level as Number;

            function initialize(level as Number) {
                // A condition for capturing a range of lines that are all indented
                Iterators.Condition.initialize();
                _level = level;
            }

            function evaluate(line as String) as Boolean {
                // Check that first `_level` chars are all tabs
                var chars = line.toCharArray();
                if (chars.size() < _level) {
                    return false;
                }
                for (var i = 0; i < _level; i++) {
                    if (chars[i] != '\t') {
                        return false;
                    }
                }
                return true;
            }

            function toString() as String {
                return "_IsIndented{" +
                    "level=" + _level.toString() + "}";
            }
        }
    }
}
