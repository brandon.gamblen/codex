import Toybox.WatchUi;


module Codex {

    class MarkdownViewDelegate extends WatchUi.BehaviorDelegate {

        protected var app as Codex.App;
        protected var view as Codex.MarkdownView;
        
        function initialize(app as Codex.App, view as Codex.MarkdownView) {
            /*
            Behaviour delegate for Codex.MarkdownView

            :param app: Application (weak reference will be kept)
            :param view: Markdown view (weak reference will be kept)
            */
            WatchUi.BehaviorDelegate.initialize();
            
            self.app = app.weak();
            self.view = view.weak();
        }

        function onKeyPressed(keyEvent as WatchUi.KeyEvent) as Boolean {
            if (keyEvent.getKey() == WatchUi.KEY_ENTER) {
                // Necessary because onSelect() is suppressed
                return onEnter();
            } else {
                return false;
            }
        }

        function onSwipe(swipeEvent as WatchUi.SwipeEvent) as Boolean {
            switch (swipeEvent.getDirection()) {
                case WatchUi.SWIPE_UP:
                    return onNextPage();
                case WatchUi.SWIPE_DOWN:
                    return onPreviousPage();
                case WatchUi.SWIPE_LEFT:
                    return onEnter();
                case WatchUi.SWIPE_RIGHT:
                    return onBack();
            }
        }

        function onNextPage() as Boolean {
            // Scroll down
            var result;
            if (!view.get().isContentScrolledToBottom()) {
                result = view.get().scrollContent(1);
                WatchUi.requestUpdate();
            } else {
                result = false;
            }
            return result;
        }

        function onPreviousPage() as Boolean {
            // Scroll up
            var result;
            if (!view.get().isContentScrolledToTop()) {
                result = view.get().scrollContent(-1);
                WatchUi.requestUpdate();
            } else {
                result = false;
            }
            return result;
        }

        function onSelect() as Boolean {
            // Suppress so input is handled separately by onKeyPressed() or onTap()
            return false;
        }

        function onBack() as Void {
            // Go back to table of contents
            return app.get().exitView();
        }

        function onMenu() as Boolean {
            // Default: not implemented
            return false;
        }
    }
}
