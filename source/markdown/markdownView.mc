import Toybox.Test;
import Toybox.Math;
import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Lang;


module Codex {

    class MarkdownView extends WatchUi.View {

        // Angle in degrees of the top-left corner above the horizontal
        // 45 degrees means that the text will be displayed across a square area
        // A smaller angle means that the display area will be wider (less lines displayed in full)
        // A larger angle means that the display area will be taller (more lines dispalyed in full)
        static const DISPLAY_ASPECT_RATIO_ANGLE = 30 * (Math.PI / 180);

        // State variables 
        private var _scrollLocation as Number;

        // Non-state variables
        protected var app as Codex.App;  // Weak reference to app

        private var _title as Symbol;
        private var _resourceId as Symbol;
        private var _theme as Theme;

        private var _screenDiameter as Number;

        private var _readableWidth as Number;
        private var _readableHeight as Number;

        private var _dc as Graphics.Dc;

        private var _xOrigin as Number;
        private var _yOrigin as Number;

        private var _parser as MarkdownParser;
        private var _body as Body;

        function initialize(app as Codex.App, title as Symbol, resourceId as Symbol, theme as Codex.Theme) as Void {
            /*
            View object allowing user to scroll through content

            :param app: Back reference to Codex.App (will be kept as weak reference)
            :param title: Title of content (resource symbol)
            :param resourceId: Markdown string resource symbol
            :param theme: Color theme
            */
            View.initialize();

            self.app = app.weak();
            _title = title;
            _resourceId = resourceId;
            _theme = theme;

            _scrollLocation = 0;

            _parser = new MarkdownParser(_resourceId, theme);
            _body = new Body(_parser);
        }

        function getDelegate() as _MarkdownViewDelegate {
            return new MarkdownViewDelegate(app.get(), self);
        }

        function getTitle() as Symbol {
            return _title;
        }

        function getResourceId() as Symbol {
            return _resourceId;
        }

        function getTheme() as Codex.Theme {
            return _theme;
        }

        function setTheme(theme as Codex.Theme) as Void {
            _theme = theme;
        }

        function onLayout(dc as Dc) as Void {

            // Get screen dims
            // Assume circular screen  (all supported devices currently have circular screens)
            if (dc.getWidth() != dc.getHeight()) {
                throw new Lang.InvalidValueException(
                    "Unsupported screen dimensions detected: width != height");
            }

            _screenDiameter = dc.getWidth();

            _readableWidth = 2 * (_screenDiameter * Math.cos(DISPLAY_ASPECT_RATIO_ANGLE) / 2).toNumber();
            _readableHeight = 2 * (_screenDiameter * Math.sin(DISPLAY_ASPECT_RATIO_ANGLE) / 2).toNumber();

            // Start displaying text at the top edge of the circle
            _xOrigin = Math.ceil((_screenDiameter - _readableWidth) / 2).toNumber();
            _yOrigin = Math.ceil((_screenDiameter - _readableHeight) / 2).toNumber();

            TextElement.computeFontHeights(dc);

            _dc = dc;
        }

        function onShow() as Void {
            
            var y = _yOrigin - _scrollLocation;
            _body.load(_dc, _readableWidth, -y, _screenDiameter - y);
        }

        function onUpdate(dc as Dc) as Void {

            dc.setColor(_theme.getTextColor(), _theme.getBackgroundColor());
            dc.clear();

            var y = _yOrigin - _scrollLocation;
            _body.draw(dc, _xOrigin, y);
        }

        function onHide() as Void {
            // Unload elements
            _body.unload();
            _parser.unload();
        }

        function scrollContent(direction as Number) as Void {
            // Scroll content downwards (direction = 1) or upwards (direction = -1)

            // Amount to search (+ve for forward, -ve for backward) for scroll point
            var scrollSearchAmount = direction * _readableHeight;

            // Pre-load content
            var y = _yOrigin - _scrollLocation;
            if (direction == 1) {
                _body.load(_dc, _readableWidth, -y, _dc.getHeight() + scrollSearchAmount - y);
            } else {
                // 2 * scrollSearchAmount chosen as a safe amount
                _body.load(_dc, _readableWidth, -y + (2 * scrollSearchAmount), _dc.getHeight() - y);
            }

            // Get scroll point within element
            var scrollPoint = _body.getScrollPoint(
                -_scrollLocation < scrollSearchAmount ? scrollSearchAmount + _scrollLocation : 0);

            // Test is scroll point overruns end of last element
            if (direction == 1 && !_body.isHeightGreaterThanOrEqualTo(scrollPoint + _readableHeight)) {
                scrollPoint = _body.getHeight() - _readableHeight;
            }

            // Compute scroll amount
            if (direction == 1 ? scrollPoint <= _scrollLocation : scrollPoint >= _scrollLocation) {
                throw new Lang.InvalidValueException(
                    "Computed scrollPoint was zero or in wrong direction");
            }
            _scrollLocation = scrollPoint;

            return true;
        }

        function resetContentScroll() as Boolean {
            // Reset scroll to top
            if (_scrollLocation != 0) {
                _body.load(_dc, _readableWidth, -_yOrigin, _screenDiameter - _yOrigin);
                _scrollLocation = 0;
                return true;
            } else {
                return false;
            }
        }

        function isContentScrolledToTop() as Boolean {
            // Check if top of first element is aligned with top of screen
            return _scrollLocation <= 0;
        }

        function isContentScrolledToBottom() as Boolean {
            // Check if bottom of last element is aligned with bottom of screen
            return _body.isHeightKnown() && _body.getHeight() - _scrollLocation <= _readableHeight;
        }
    }
}
