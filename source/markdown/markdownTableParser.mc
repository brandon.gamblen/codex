import Toybox.Lang;

import Iterators;
import Strings;


module Codex {

    class MarkdownTableParser extends Iterators.MemoIterator {

        enum {
            STATE_SOF,
            STATE_HEADER,
            STATE_HYPHEN,
            STATE_CONTENT,
            STATE_EOF,
        }

        private var _lines as Iterators.BaseIterator;
        private var _theme as Theme;

        private var _state as Number;

        function initialize(lines as Iterators.BaseIterator, theme as Theme) {
            /*
            An iterator for parsing a markdown table into a series of table rows

            :param lines: Iterator of Markdown lines
            :param theme: Styling options
            */
            MemoIterator.initialize();

            _lines = lines;
            _theme = theme;

            _state = STATE_SOF;
        }

        protected function nextImpl() as TableRow? {
            // Implementation of next()
            while(_state != STATE_EOF) {

                var nextLine = _lines.next();
                var nextState;

                var result;

                if (nextLine == null) {
                    // No more table lines left
                    nextState = STATE_EOF;
                    result = null;
                } else {
                    switch(_state) {
                        case STATE_SOF:
                            nextState = STATE_HEADER;
                            result = _parseContentRow(nextLine, true);
                            break;
                        case STATE_HEADER:
                            nextState = STATE_HYPHEN;
                            result = _parseHyphenRow(nextLine);
                            break;
                        default:
                            nextState = STATE_CONTENT;
                            result = _parseContentRow(nextLine, false);
                            break;
                    }
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        private static function _parseHyphenRow(str as String) as Void {
            // TODO: use this information to determine content alignment
            // Return: number of cells
            // TODO: implement
        }

        private static function _parseContentRow(str as String, isHeaderRow as Boolean) as TableRow {

            var cells = [];
            var cellStrs = new Strings.Splitter(str, "|");
            var state = :startOfRow;

            while (state != :endOfRow && cellStrs.hasNext()) {
                var cellStr = cellStrs.next();
                switch(state) {
                    case :startOfRow:
                        if (cellStr.length() > 0) {
                            throw new Lang.InvalidValueException(
                                "Each table row must begin with a leading '|' character");
                        }
                        state = :cell;
                        break;
                    case :cell:
                        if (cellStrs.hasNext()) {
                            var content = _parseCellContent(cellStr);
                            if (isHeaderRow) {
                                cells.add(new TableHeader(content, _theme));
                            } else {
                                cells.add(new TableCell(content, _theme));
                            }
                        } else {
                            if (cellStr.length() == 0) {
                                state = :endOfRow;
                            } else {
                                throw new Lang.InvalidValueException(
                                    "Each table row must end with a trailing '|' character");
                            }
                        }
                        break;
                }
            }

            if (state != :endOfRow || cellStrs.hasNext()) {
                throw new Lang.InvalidValueException(
                    "Invalid table row: \"" + str + "\"");
            }

            if (cells.size() == 0) {
                throw new Lang.InvalidValueException(
                    "Each table row must have at least one cell");
            }

            return new TableRow(new Iterators.ArrayIterator(cells), _theme);
        }

        private static function _parseCellContent(str as String) as Element {
            str = Strings.trim(str, false);
            var state = MarkdownParser.getState(str);
            switch (state) {
                case MarkdownParser.STATE_WHITESPACE:
                case MarkdownParser.STATE_PARAGRAPH:
                    return MarkdownParser.parseParagraph(str, _theme);
                case MarkdownParser.STATE_HEADING:
                    return MarkdownParser.parseHeading(str, _theme);
                case MarkdownParser.STATE_IMAGE:
                    return MarkdownParser.parseImage(str, _theme);
                default:
                    throw new Lang.InvalidValueException(
                        "Unable to parse table cell content: \"" + str + "\"");
            }
        }
    }
}
