import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;


module Codex {

    class App extends Application.AppBase {

        // Color themes
        static const THEME_DARK = new Codex.Theme(
            0x000000,   // Background color
            0xFFFFFF,   // Font color
            0xCFCFCF,   // Accent color (major)
            0x2F2F2F,   // Accent color (minor)
            {}          // Options
        );
        static const THEME_LIGHT = new Codex.Theme(
            0xFFFFFF,   // Background color
            0x000000,   // Font color
            0x2F2F2F,   // Accent color (major)
            0xCFCFCF,   // Accent color (minor)
            {}          // Options
        );

        private var _viewStack as Array<WatchUi.View>;
        private var _partialUpdateFlag as Boolean;

        function initialize(mainMenu as Codex.Menu, resourceSymbolTable as Dictionary) {
            AppBase.initialize();

            _viewStack = [mainMenu];
            _partialUpdateFlag = false;

            // Ugh
            Image.setResourceSymbolTable(resourceSymbolTable);
        }

        function onStart(state as Dictionary?) as Void {
            // Do nothing
        }

        function onStop(state as Dictionary?) as Void {
            // Do nothing
        }

        function requestPartialUpdate() as Void {
            _partialUpdateFlag = true;
            WatchUi.requestUpdate();
        }

        function isPartialUpdate() as Boolean {
            if (_partialUpdateFlag) {
                // Clear flag
                _partialUpdateFlag = false;
                return true;
            } else {
                return false;
            }
        }

        function getInitialView() as Array<WatchUi.View or WatchUi.InputDelegate>? {
            var currentView = getCurrentView();
            return [currentView, currentView.getDelegate()] as Array<WatchUi.View or WatchUi.InputDelegate>;
        }

        function getCurrentView() as WatchUi.View {
            // Peek into view stack
            return _viewStack[_viewStack.size() - 1];
        }

        function isAppAtMainMenu() as Boolean {
            // Check if app is at top-level menu
            return _viewStack.size() == 1;
        }

        function popView(transition as WatchUi.SlideType) as Void {
            // Wrap WatchUi.popView()
            _viewStack = _viewStack.slice(0, _viewStack.size() - 1);
            WatchUi.popView(transition);
        }

        function pushView(view as WatchUi.View, transition as WatchUi.SlideType) as Void {
            // Wrap WatchUi.pushView()
            _viewStack.add(view);
            WatchUi.pushView(view, view.getDelegate(), transition);
        }

        function switchToView(view as WatchUi.View, transition as WatchUi.SlideType) as Void {
            // Wrap WatchUi.switchToView()
            _viewStack[_viewStack.size() - 1] = view;
            WatchUi.switchToView(view, view.getDelegate(), transition);
        }

        function selectView(view as WatchUi.View) as Boolean {
            /*
            Select view from menu

            :param view: View object
            */
            if (view instanceof Codex.Menu) {
                pushView(view, WatchUi.SLIDE_LEFT);
            } else {
                pushView(view, WatchUi.SLIDE_IMMEDIATE);
            }
        }

        function exitView() as Boolean {
            // Return to menu from view
            // If view is main menu, exit app
            if (isAppAtMainMenu()) {
                return _exitApp();
            } else {
                return _exitView();
            }
        }

        private function _exitView() as Boolean {
            // Return to menu from sub-menu or leaf view

            var currentView = getCurrentView();

            var title = currentView.getTitle();
            if (currentView instanceof Codex.Menu) {
                popView(WatchUi.SLIDE_RIGHT);
            } else {
                popView(WatchUi.SLIDE_IMMEDIATE);
            }

            var menu = getCurrentView();
            menu.setItemFocus(title);
            
            return true;
        }

        private function _exitApp() as Boolean {
            // Exit app using back button
            popView(WatchUi.SLIDE_IMMEDIATE);
            return true;
        }
    }
}
