import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;


module Codex {

    class Menu extends WatchUi.View {

        static const ITEM_PADDING_SIDES = 4;

        // State variables
        private var _focus as Number?;

        // Non-state variables
        protected var app as Codex.App;

        private var _title as Symbol;
        private var _items as Array<Symbol>;
        private var _content as Dictionary;
        private var _theme as Theme;

        private var _titleString as String;
        private var _itemStrings as Array<String>;

        private var _text as WatchUi.Text;
        
        function initialize(app as Codex.App, title as Symbol, items as Array<Symbol>, content as Dictionary,
            theme as Codex.Theme) as Void {
            /*
            Base menu class

            Handles selecting elements and pushing and popping views from the stack

            :param app: Codex.App
            :param title: Menu title resource symbol
            :param items: Array of menu item resource symbols
            :param content: Dictionary of menu content, mapping item ID -> View OR 
                Dictionary containing :method and :options, i.e. a factory
            :param theme: Theme
            */
            WatchUi.View.initialize();

            self.app = app.weak();

            _title = title;
            _items = items;
            _content = content;
            _theme = theme;

            // Load resource strings
            _titleString = WatchUi.loadResource(title);
            _itemStrings = [];
            for (var i = 0; i < items.size(); i++) {
                _itemStrings.add(WatchUi.loadResource(items[i]));
            }

            _focus = 0;
        }

        function getDelegate() as Codex.MenuDelegate {
            return new Codex.MenuDelegate(app.get(), self);
        }

        function getNumItems() as Number {
            return _items.size();
        }

        function setFocus(focus as Number?) as Void {
            _focus = focus;
        }

        function getFocus() as Number? {
            return _focus;
        }

        function setItemFocus(item as Symbol?) as Void {
            /*
            Set focus of menu

            :param item: Item to focus on (resource symbol)
            */
            if (item != null) {
                _focus = _items.indexOf(item);
                if (_focus == -1) {
                    throw new Lang.InvalidValueException(
                        "setItemFocus() called with invalid item ID: " + item.toString());

                }
            } else {
                _focus = null;
            }
        }

        function getTitle() as Symbol {
            return _title;
        }

        function getItem(index as Number) as Symbol {
            return _items[index];
        }

        function getContent(item as Symbol) as WatchUi.View {
            var entry = _content[item];
            if (entry instanceof WatchUi.View) {
                return entry;
            } else if (entry instanceof Dictionary) {
                var view = entry[:method].invoke(entry[:options]) as WatchUi.View;
                _content[item] = view;
                return view;
            } else {
                throw new Lang.InvalidValueException(
                    "content must be a View or Dictionary");
            }
        }

        function setTheme(theme as Codex.Theme) as Void {
            // Set styling options
            _theme = theme;
        }

        function onUpdate(dc as Graphics.Dc) as Void {

            var backgroundColor = _theme.getBackgroundColor();

            var titleHeight = _theme.getMenuTitleHeight();
            var titleFont = _theme.getMenuTitleFont();
            var titleTextColor = _theme.getMajorAccentColor();

            var itemHeight = _theme.getMenuItemHeight();
            var itemFont = _theme.getMenuItemFont();
            var itemTextColor = _theme.getTextColor();
            var itemBorderColor = _theme.getMajorAccentColor();

            var focusHeight = _theme.getMenuFocusHeight();
            var focusFont = _theme.getMenuFocusFont();
            var focusTextColor = _theme.getBackgroundColor();
            var focusBackgroundColor = _theme.getTextColor();
            
            var displayWidth = dc.getWidth();
            var displayHeight = dc.getHeight();

            // Clear screen
            dc.setColor(titleTextColor, backgroundColor);
            dc.clear();

            // Draw title
            var titleY = (_focus != null) ? -_focus * itemHeight : 0;
            if (titleY + titleHeight >= 0) {
                // Draw title text
                dc.drawText(displayWidth / 2, titleY + (titleHeight / 2), titleFont, _titleString,
                    Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);

                // Draw title bottom border
                dc.setColor(itemBorderColor, backgroundColor);
                dc.setPenWidth(1);
                dc.drawLine(0, titleY + titleHeight, displayWidth, titleY + titleHeight);
            }

            // Draw menu items
            var itemY = titleY + titleHeight;
            for (var i = 0; i < _items.size(); i++) {
                if (itemY >= displayHeight) {
                    break;
                }
                if (_focus != null && i == _focus) {
                    if (itemY + focusHeight >= 0) {
                        // Fit text
                        var itemString = (new Strings.TextWrapper(dc, _itemStrings[i], displayWidth - 2 * ITEM_PADDING_SIDES, focusFont)).next();

                        // Fill item
                        dc.setColor(focusBackgroundColor, backgroundColor);
                        dc.fillRectangle(0, itemY, displayWidth, focusHeight);

                        // Draw text
                        dc.setColor(focusTextColor, focusBackgroundColor);
                        dc.drawText(displayWidth / 2, itemY + focusHeight / 2, focusFont, itemString,
                            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
                    }
                    itemY += focusHeight;
                } else {
                    if (itemY + itemHeight >= 0) {
                        // Fit text
                        var itemString = (new Strings.TextWrapper(dc, _itemStrings[i], displayWidth - 2 * ITEM_PADDING_SIDES, itemFont)).next();

                        // Draw text
                        dc.setColor(itemTextColor, backgroundColor);
                        dc.drawText(displayWidth / 2, itemY + itemHeight / 2, itemFont, itemString,
                            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);

                        // Draw bottom border
                        dc.setColor(itemBorderColor, backgroundColor);
                        dc.drawLine(0, itemY + itemHeight, displayWidth, itemY + itemHeight);
                    }
                    itemY += itemHeight;
                }
            }
        }

        function getItemByCoordinates(coords as Array<Number>) as Symbol? {
            // Get menu item by pixel coordinates
            // Used to handle ClickEvents
            var titleHeight = _theme.getMenuTitleHeight();
            var itemHeight = _theme.getMenuItemHeight();
            var focusHeight = _theme.getMenuFocusHeight();

            var y = coords[1];
            var itemY = ((_focus != null) ? -_focus * itemHeight : 0) + titleHeight;

            for (var i = 0; i < _items.size(); i++) {
                var height = (_focus != null && i == _focus) ? focusHeight : itemHeight;
                if (y >= itemY && y < itemY + height) {
                    return _items[i];
                }
                itemY += height;
            }
            return null;
        }
    }
}
