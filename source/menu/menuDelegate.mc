import Toybox.WatchUi;
import Toybox.Math;
import Toybox.System;


module Codex {

    class MenuDelegate extends WatchUi.BehaviorDelegate {

        protected var app as Codex.App;
        protected var menu as Codex.BaseMenu;
        
        function initialize(app as Codex.App, menu as Codex.BaseMenu) as Void {
            /*
            :param app: Codex.App (a weak reference will be kept)
            :param menu: Codex.BaseMenu (a weak reference will be kept)
            */
            WatchUi.BehaviorDelegate.initialize();

            self.app = app.weak();
            self.menu = menu.weak();
        }

        function onKeyPressed(keyEvent as WatchUi.KeyEvent) as Boolean {
            if (keyEvent.getKey() == WatchUi.KEY_ENTER) {
                // Necessary because onSelect() is suppressed
                return onEnter();
            } else {
                return false;
            }
        }

        function onTap(clickEvent as WatchUi.ClickEvent) as Boolean {
            var item = menu.get().getItemByCoordinates(clickEvent.getCoordinates());
            if (item != null) {
                var view = menu.get().getContent(item);
                return app.get().selectView(view);
            } else {
                return false;
            }
        }

        function onSwipe(swipeEvent as WatchUi.SwipeEvent) as Boolean {
            switch (swipeEvent.getDirection()) {
                case WatchUi.SWIPE_UP:
                    return onNextPage();
                case WatchUi.SWIPE_DOWN:
                    return onPreviousPage();
                case WatchUi.SWIPE_LEFT:
                    return onEnter();
                case WatchUi.SWIPE_RIGHT:
                    return onBack();
            }
        }

        function onNextPage() as Boolean {
            var focus = menu.get().getFocus();
            if (focus != null ) {
                if (focus == menu.get().getNumItems() - 1) {
                    // Wrap
                    focus = 0;
                } else {
                    focus ++;
                }
                menu.get().setFocus(focus);
                WatchUi.requestUpdate();
                return true;
            } else {
                return false;
            }
        }

        function onPreviousPage() as Boolean {
            var focus = menu.get().getFocus();
            if (focus != null) {
                if (focus == 0) {
                    // Wrap focus
                    focus = menu.get().getNumItems() - 1;
                } else {
                    focus --;
                }
                menu.get().setFocus(focus);
                WatchUi.requestUpdate();
                return true;
            } else {
                return false;
            }
        }

        function onEnter() as Boolean {
            // Select button was pressed or screen was swiped left
            var focus = menu.get().getFocus();
            if (focus != null) {
                var item = menu.get().getItem(focus);
                var view = menu.get().getContent(item);
                return app.get().selectView(view);
            } else {
                return false;
            }
        }

        function onSelect() as Boolean {
            // Suppress so input is handled separately by onKeyPressed() or onTap()
            return false;
        }

        function onBack() as Boolean {
            return app.get().exitView();
        }

        function onMenu() as Boolean {
            // Default: not implemented
            return false;
        }
    }
}
