import Toybox.Graphics;


module Codex {
    
    (:test)
    function test_load_time_long_paragraph(logger as Logger) as Boolean {
        // Test that loading/unloading a long paragraph does not trigger WatchdogTrippedError
        var theme = App.THEME_LIGHT;

        var parser = new MarkdownParser(Rez.Strings.Codex_TestString_long_paragraph, theme);
        var body = new Body(parser);

        var width = 226;
        var height = 260;
        var buffer = 130;
        var scrollAmount = 110;
        var numScroll = 20;

        var screen = new Graphics.BufferedBitmap({
            :width => width,
            :height => height,
        });

        var dc = screen.getDc();
        
        TextElement.computeFontHeights(dc);

        for (var i = 0; i < numScroll; i++) {
            var scrollOffset = scrollAmount * i;
            body.load(dc, width, scrollOffset, height + buffer + scrollOffset);
        }

        return true;
    }
}




